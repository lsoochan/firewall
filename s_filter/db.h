#ifndef _DB_H

#define _DB_H		1

#include <postgresql/libpq-fe.h>

#define DB_HOST	"127.0.0.1"
#define DB_NAME	"privacycenter"
#define DB_PORT	"5432"

#define NUM_TUPLES( result ) PQntuples( result )

#define DB_STATUS_OK( x ) x >= 0

enum { DB_QUERY_FAIL = -999, DB_CONN_ERR, DB_CONN_FAIL, DB_CONN_OK = 1, DB_QUERY_OK };  // Any failure values must be less than zero.

int dbinit();
int dbconnok();
int dbstatus();
int dbreset();
void dbclose();

int setstatus(int dbstatno);

inline int dbexec_no_result(const char *sql_fmt, ...);
inline PGresult *dbexec_result(const char *sql_fmt, ...);

extern char *sql__select_action_url;
extern char *sql__select_board[];
extern char *sql__insert_http;
extern char *sql__select_basic_option;
extern char *sql__update_pass;
extern char *sql__delete_http;
extern char *sql__delete_fields;
extern char *sql__insert_afile;
extern char *sql__select_afile;
extern char *sql__delete_afile;
extern char *sql__select_duplicate;
extern char *sql__select_duplicate_board;
extern char *sql__select_duplicate_user;
extern char *sql__select_duplicate_board_user;
extern char *sql__delete_duplicate;
extern char *sql__insert_duplicate;
extern char *sql__block_file;
extern char *sql__block_file_size;
extern char *sql__block_file_ext;
extern char *sql__block_src_ip;
extern char *sql__insert_blocking_content;
extern char *sql__select_option;

#endif
