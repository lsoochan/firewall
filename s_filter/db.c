#include <syslog.h>
#include <stdarg.h>
#include <postgresql/libpq-fe.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>


#include "db.h"

#define SMALL_BUF_LEN       256
#define BUF_LEN             1024 * 1024 // 1M
#define LARGE_BUF_LEN       10485760    // 10M

#define SQL_BUF_LEN         BUF_LEN

static int DB_RESULT_CNT = 0;
static PGconn *conn = NULL;
static char sql_buf[SQL_BUF_LEN];
static int dbstatno;

char *sql__insert_blocking_content =
    "insert into http_filtered_data (http_log_id, content, reason) values ( %llu,'%s',%d)";
char *sql__select_option =
    " select option.* from filtering_privacy_option option, "
    " filtering_board_info info  where info.id = %d and info.privacy_option_id = option.id ";

char *sql__select_action_url =
    " select distinct action_url from server_info, server_admin_mapping, "
    " filtering_board_info, filtering_board_inspect "
    " where server_info.id = server_admin_mapping.server_sid and "
    " server_admin_mapping.admin_id = filtering_board_info.admin_id and "
    " filtering_board_info.id = filtering_board_inspect.board_id and "
    " server_info.pid = %d  and " " filtering_board_inspect.in_use = 1 ";


char *sql__select_board[2] = {
    // allow urls
    " select category_param_name, category_param_value"
        "   from filtering_board_allow where in_use = 1 and action_url = \'%s\' ",

    // inspect urls
    " select category_param_name, category_param_value, board_id, userid_param, subject_param, "
        " content_param from filtering_board_inspect where in_use = 1 and action_url = \'%s\' "
};

char *sql__insert_http =
    "insert into http_log(sid, id,pass,pstore_id, request, src_ip, dst_ip, src_port, "
    " dst_port, dates, board_id, exist_file, board_alert, recovery, host, "
    " url, referer, user_id, subject, body ) "
    " values ( %d, %llu, %d, %d, %d, \'%s\', \'%s\', %u, "
    " %u, now(), %u, %u, 0, 0, \'null\', "
    " \'%s\', \'%s\', \'%s\', \'%s\', \'%s\' ) ";

char *sql__select_basic_option =
    "select afile_self, afile_size, text_size, filtering_grade, duplicate_block_use, duplicate_block_period, duplicate_block_limit, default_filtering_option, default_filtering_subject_param, default_filtering_body_param, default_filtering_writer_param, replace_use from filtering_basic_option";

char *sql__update_pass =
    "update http_log set pass = %u where sid = %d and id = %llu ";

char *sql__delete_http =
    "delete from http_log where sid = %d and id = %llu ";

char *sql__delete_fields =
    "delete from http_params where sid = %d and id = %llu ";

char *sql__insert_afile =
    "insert into http_attach_files ( sid, http, type, name, path, orig_path ) "
    "values ( %d, %llu, %u, \'%s\', \'%s\', \'%s\' )";
char *sql__select_afile =
    "select id, path, orig_path from http_attach_files where sid = %d and http = %llu ";

char *sql__delete_afile =
    "delete from http_attach_files where sid = %d and http = %llu ";

char *sql__select_duplicate =
    "select count(*) from duplicate where content_hash = '%s' and subject_hash = '%s' and"
    " content_len = %d and afile_size = %d and time > (now() - interval '%d minutes')";
char *sql__select_duplicate_board =
    "select count(*) from duplicate where content_hash = '%s' and subject_hash = '%s' and"
    " content_len = %d and afile_size = %d and boardid = %d and time > (now() - interval '%d minutes')";
char *sql__select_duplicate_user =
    "select count(*) from duplicate where content_hash = '%s' and subject_hash = '%s' and"
    " content_len = %d and afile_size = %d and userid = '%s' and ip = '%s' and time > (now() - interval '%d minutes')";
char *sql__select_duplicate_board_user =
    "select count(*) from duplicate where content_hash = '%s' and subject_hash = '%s' and "
    " content_len = %d and afile_size = %d and boardid = %d and userid = '%s' and ip = '%s' and "
    " time > (now() - interval '%d minutes')";


char *sql__delete_duplicate =
    "delete from duplicate where time <= (now() - interval '%d minutes')";

char *sql__insert_duplicate =
    "insert into duplicate (boardid, userid, subject_hash, content_hash, "
    " content_len, afile_size, ip) values(%d, \'%s\', \'%s\', \'%s\', %d, %d, \'%s\')";

char *sql__block_file =
    "insert into http_filtered_data (http_log_id, content, reason) values "
    " ( %llu, '%s', %d)";

char *sql__block_file_size =
    "insert into http_filtered_data (http_log_id, content, reason) values "
    " ( %llu, 'Total size of %d files is larger than its limit', %d)";

char *sql__block_file_ext =
    "insert into http_filtered_data (http_log_id, content, reason) values "
    " ( %llu, '%s', %d)";

char *sql__block_src_ip =
    "insert into http_filtered_data (http_log_id, content, reason) values "
    " ( %llu, '%s', %d)";



int dbinit(char *db_port)
{
    char conninfo[SMALL_BUF_LEN];

    sprintf(conninfo, "hostaddr=%s dbname=%s port=%s", DB_HOST, DB_NAME,
            db_port);
    syslog(LOG_DEBUG, "DB connection = [%s] ", conninfo);

    conn = PQconnectdb(conninfo);

    if (conn == NULL) {
        setstatus(DB_CONN_FAIL);
        return 0;
    }

    if (!dbconnok()) {
        setstatus(DB_CONN_ERR);
        return 0;
    }

    setstatus(DB_CONN_OK);

    return 1;
}

int setstatus(int statno)
{
    dbstatno = statno;
    return 1;
}


int dbstatus()
{
    switch (dbstatno) {
    case DB_QUERY_OK:
        syslog(LOG_DEBUG, "Query executed successfully");
        break;
    case DB_QUERY_FAIL:
        syslog(LOG_WARNING, "Query failed to be excuted");
        break;
    case DB_CONN_OK:
        syslog(LOG_DEBUG, "DB CONNECTION OK");
        break;
    case DB_CONN_FAIL:
        syslog(LOG_WARNING, "PQconnectdb() failed.");
        break;
    case DB_CONN_ERR:
        syslog(LOG_WARNING, "PQstatus() != CONNECTION_OK");
        break;
    }

    return dbstatno;
}

int dbconnok()
{
    syslog(LOG_DEBUG, "DB connection checking...");
    if (PQstatus(conn) != CONNECTION_OK) {
        setstatus(DB_CONN_ERR);
        dbstatus();
        return 0;
    }

    setstatus(DB_CONN_OK);
    dbstatus();
    return 1;
}

int dbreset()
{
    PQreset(conn);
    return 1;
}

void dbclose()
{
    if (conn != NULL) {
        PQfinish(conn);
        conn = NULL;
    }
}

inline int dbexec_no_result(const char *sql_fmt, ...)
{
    PGresult *res;
    va_list ap;

    va_start(ap, sql_fmt);
    vsprintf(sql_buf, sql_fmt, ap);
    va_end(ap);

    res = PQexec(conn, sql_buf);

    if (NULL == res || PGRES_FATAL_ERROR == PQresultStatus(res)
        || PGRES_EMPTY_QUERY == PQresultStatus(res)) {
        syslog(LOG_WARNING,
               "Query fails without return [%s] : [error code: %d]",
               sql_buf, PQresultStatus(res));
        setstatus(DB_QUERY_FAIL);
        PQclear(res);
        return (0);
    }


    PQclear(res);

    setstatus(DB_QUERY_OK);
    return (1);
}


inline PGresult *dbexec_result(const char *sql_fmt, ...)
{
    PGresult *res;
    va_list ap;

    va_start(ap, sql_fmt);
    vsprintf(sql_buf, sql_fmt, ap);
    va_end(ap);

    //  syslog( LOG_WARNING, "dbexec_result : [%s]", sql_buf);
    res = PQexec(conn, sql_buf);

    if (NULL == res || PGRES_FATAL_ERROR == PQresultStatus(res) ||
        PGRES_EMPTY_QUERY == PQresultStatus(res)) {
        syslog(LOG_WARNING,
               "Query fails with return [%s] : [error code: %d]", sql_buf,
               PQresultStatus(res));
        setstatus(DB_QUERY_FAIL);
        PQclear(res);
        return (NULL);
    }

    setstatus(DB_QUERY_OK);
    DB_RESULT_CNT++;
    return (res);
}



inline void dbexec_release(PGresult * res)
{
    if (res != NULL) {
        syslog(LOG_WARNING,
               "There are %d more result set left in unfreed.",
               DB_RESULT_CNT - 1);
        DB_RESULT_CNT--;
        PQclear(res);
    }
}
