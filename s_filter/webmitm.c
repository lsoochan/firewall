//#include "config.h"

#include <sys/param.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#include <err.h>
#include <errno.h>
#include <signal.h>
//#include <sys/signalfd.h>
#include <sys/prctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <unistd.h>
#include <fcntl.h>

#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/stat.h>

#include <netdb.h>
#include <arpa/inet.h>
#include <inttypes.h>           // PRIu32

#include "buf.h"
#include "version.h"
#include "log.h"


#include <semaphore.h>

#include <db.h>

#define	SHM_KEY		1058

#define IP_TRANSPARENT	19

#define	  CERT_FILE	"/usr/local/privacycenter/etc/web.pem"
//#define   CERT_CHAIN  "/usr/local/privacycenter/etc/web.crt"
#ifdef __64bit_LOADER__
#define   MAX_LOADER       10
#else
#define   MAX_LOADER		5
#endif

#define   LOADER0_PORT  7071
#define   LOADER1_PORT  7072
#define   LOADER2_PORT  7073
#define   LOADER3_PORT  7074
#define   LOADER4_PORT  7075
#ifdef __64BIT_LOADER__
#define   LOADER5_PORT  7076
#define   LOADER6_PORT  7077
#define   LOADER7_PORT  7078
#define   LOADER8_PORT  7079
#define   LOADER9_PORT  7080
#endif


#define	  LISTEN_PORT 8443

#define   SSL	0

int log_level = PV_LOG_LEVEL;

static sem_t http_id_sem;

int https_fd;
int client_fd, server_fd;
#if SSL
SSL_CTX *ssl_client_ctx, *ssl_server_ctx;
SSL *ssl_client, *ssl_server;
#endif
struct sockaddr_in clientin, serverin;
in_addr_t static_host = 0;


static unsigned int socket_to_loader;

int read_pass_or_block(const char *);
static void server_init(char *buf, int size);
void add_x_forward_for(char *buf, int len);

static u_int32_t *_http_id;
static u_int32_t *pv_id;
static u_int32_t *hi_id;

// for loader response
static u_int64_t https_id64 = 0;        // use in read_pass_or_block func. 
int loader_response = 0;
int https_request_pass = PASS;
int proxy_timeout = 0;
int child_close = 0;

int mitm_port = 443;
int server_port = 443;
char db_port[6] = { 0x00, };
char cert_file[256] = { 0x00, };

int forwarded = 0;
int transparent = 0;
int nonblock = 0;
int max_packet_size = MAX_BUF_SIZE;
int already_sent = 0;

static void sig_proxy_timeout(int sig)
{
    alarm(0);
    proxy_timeout = 1;
}

static void sig_giveup(int sig)
{
    err(1, "client hello failed");
}

static void process_loader_finished()
{
    int pass_tmp = PASS;
    char fname[65];

    syslog(LOG_DEBUG, "loader_response");
#ifdef __64BIT_LOADER__
    snprintf(fname, 64, "%s%lu", WREQUEST_INSPECTION, https_id64);
#else
	snprintf(fname, 64, "%s%llu", WREQUEST_INSPECTION, https_id64);
#endif

    if ((pass_tmp = read_pass_or_block(fname)) < 0) {
        syslog(LOG_DEBUG, "read_pass_or_block failed. POST pass!!");
        https_request_pass = PASS;
    }

    loader_response = 1;
    https_request_pass = pass_tmp;
}

static void sig_loader_process_finish(int sig)
{
	loader_response = 1;
    process_loader_finished();
}

static volatile int nchilds = 0;
static void sig_chld(int signal)
{
    while (waitpid(-1, NULL, WNOHANG) > 0) {
        nchilds--;
    }
}

static volatile int exit_request = 0;
static void sig_int(int signal)
{
    exit_request = 1;
}

// notice file
static char *notice;

static char *notice_hdr =
    "HTTP/1.1 200 OK\r\n"
    "Transfer-Encoding: chunked\r\n"
    "Content-Type: text/html; charset=euc-kr\r\n"
    "Cache-Control: no-cache\r\n"
    "Pragma: no-cache\r\nExpires: -1\r\n\r\n";

unsigned int get__hi_id(int pv_id)
{
    char *ptr;
    unsigned int hi_id;
    PGresult *res = NULL;

    res =
        dbexec_result("select hi_id from hi_id where pv_id = %d ", pv_id);

    if (NULL == res) {
        syslog(LOG_WARNING, "get__hi_id() : query fails ");
        return (0);
    }

    if (0 < PQntuples(res)) {
        ptr = PQgetvalue(res, 0, 0);
        hi_id = atoi(ptr);
        PQclear(res);
    } else {
        PQclear(res);

        hi_id = 0;

        dbexec_no_result("insert into hi_id values ( %d, %d, now() ) ",
                         pv_id, hi_id);
    }

    return (++hi_id);
}

// make @@ to id64, ## to ui_ip
int make_notice(char *buf)
{
    char *id_64;
    static char buffer[8398];
    int index = 0, i = 0;
    int notice_len;

    id_64 = malloc(sizeof(char) * 24);

    notice_len = strlen(notice);
#ifdef __64BIT_LOADER__
    sprintf(id_64, "%lu", https_id64);
#else
	sprintf(id_64, "%llu", https_id64);
#endif

    while (index <= notice_len) {
        if (notice[index] == '@') {
            if (notice[index + 1] == '@') {
                strcpy(buffer + i, id_64);
                i += strlen(id_64);
                index += 2;
            }
        } else {
            buffer[i++] = notice[index++];
        }
    }

    sprintf(buf, "%s%.4x\r\n%s%s", notice_hdr, strlen(buffer), buffer,
            "\r\n0\r\n\r\n");
    notice_len = strlen(buf);

    free(id_64);
    return notice_len;
}

void read_notice_file()
{
    FILE *f = fopen(NOTICE_FILE, "r");
    char c;
    int i = 0;

    i = 0;
    while ((c = fgetc(f)) != EOF) {
        notice[i++] = c;
    }
    notice[i] = 0;

    fclose(f);
}

u_int32_t get__http_id(void)
{
    u_int32_t http_id;

    syslog(LOG_DEBUG, "get__http_id *_http_id = [%" PRIu32 "]", *_http_id);
    sem_wait(&http_id_sem);
    http_id = (*_http_id)++;
    sem_post(&http_id_sem);
    syslog(LOG_DEBUG, "get__http_id *_http_id = [%" PRIu32 "]", *_http_id);
    return (http_id);
}

struct _http *alloc_https_buf()
{
    struct _http *https;

    https = (struct _http *) malloc(sizeof(struct _http) + MAX_UDP_SIZE);

    return https;
}

void free_https_buf(void *https)
{
    free(https);
    return;
}

int init__to_loader()
{
    struct sockaddr_in sin;

    if ((socket_to_loader = socket(PF_INET, SOCK_DGRAM, 0)) < 0) {
        syslog(LOG_WARNING, "err socket");
        return -1;
    }

    syslog(LOG_DEBUG, "socket OK \n socket_to_loader = [%d]", 
                      socket_to_loader);

    /* Now bind the socket */

    memset(&sin, 0, sizeof(sin));

    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = inet_addr("127.0.0.1");
    sin.sin_port = htons(7070);
    return 0;
}

void exit__to_loader(int *socket_to_loader)
{
    close(*socket_to_loader);
}

int check_method(char *buf)
{
    if (memcmp(buf, "POST", 4) == 0) {
        return POST;
    } else {
        return OTHERS;
    }
}

/* [sclee][130425] check if it is allowed url */
int is_allowed_URL(char *buf)
{
	char *url = buf;
	int url_size;
	char *p1;
	char *string;
	int i;
	unsigned int action_url_len = 0;
	int url_cnt = 0;
	PGresult *res = dbexec_result( "select action_url from filtering_board_allow where in_use = 1" );
	if(NULL == res) {
		syslog(LOG_DEBUG, "is_allowed_URL() : db result fail");
		return NOT_ALLOW;
	}
	url_cnt = PQntuples(res);
	if( 0 == url_cnt ) {
		syslog(LOG_DEBUG, "is_allowed_URL() : there is no url list");
		PQclear(res);
		return NOT_ALLOW;
	}

	
	
	url += 5;
	if(NULL == (p1 = strchr(url, ' '))) {
		syslog(LOG_DEBUG, "is_allowed_URL() : there id no url end");
		PQclear(res);
		return NOT_ALLOW;
	}
	url_size = p1 - url;

	string = url;
	for(i = 0; i < url_cnt; i++) {
		if('?' == *(string +i)) {
			action_url_len = i;
		}		
		if(strncmp(" HTTP/1.1", string + i, 9) == 0) {
			url_size = i;
			break;
		}
	}

	if(0 == url_size)
		return NOT_ALLOW;
	
	for(i = 0; i < url_cnt; i++) {
		if( (url_size == strlen(PQgetvalue(res, i, 0))) && (!strncmp(url, PQgetvalue(res, i, 0), url_size)) ) {
			syslog(LOG_DEBUG, "is_allowed_url() : allow url match");
			PQclear(res);
			return ALLOW;
		}
	}	
		
	PQclear(res);
	return NOT_ALLOW;
}

int is_web_server_ip_not_filtering(u_int32_t dst_ip) {
	PGresult *res;
	int ip_cnt = 0;
	int i = 0;

	res = dbexec_result("select ip from filtering_board_web_server_ip where is_block = 0 and in_use = 1");
	
	if (NULL == res) {
		syslog(LOG_DEBUG,"is_web_server_ip_not_filtering() : db fail");
		return NOT_ALLOW;
	}	

	ip_cnt = PQntuples(res);
	if (0 == ip_cnt) {
		PQclear(res);
		return NOT_ALLOW;
	}

	for(i = 0; i < ip_cnt; i++) {
		if (dst_ip == inet_addr(PQgetvalue(res, i, 0))) {
			syslog(LOG_DEBUG,"is_web_server_ip_not_filtering() : allow ip match : %s", PQgetvalue(res, i, 0));
			PQclear(res);
			return ALLOW;
		}
	}

	PQclear(res);
	return NOT_ALLOW;
}

int is_web_server_ip(u_int32_t dst_ip) {
	PGresult *res;
	int ip_cnt = 0;
	int i = 0;

	res = dbexec_result("select ip from filtering_board_web_server_ip where is_block = 1 and in_use = 1");
	
	if (NULL == res) {
                syslog(LOG_DEBUG,"is_web_server_ip() : db fail");
                return NOT_ALLOW;
        }

        ip_cnt = PQntuples(res);
        if (0 == ip_cnt) {
		PQclear(res);
                return NOT_ALLOW;
        }

	 for(i = 0; i < ip_cnt; i++) {
                if (dst_ip == inet_addr(PQgetvalue(res, i, 0))) {
                        syslog(LOG_DEBUG,"is_web_server_ip() : filter ip match : %s", PQgetvalue(res, i, 0));
                        PQclear(res);
                        return NOT_ALLOW;
                }
        }

	PQclear(res);	
	return ALLOW;
}

/* [sclee][130425] check if it is allowed source ip */
int is_allowed_src_IP(u_int32_t src_ip)
{
	PGresult *res;
	int ip_cnt = 0;
	int i;

	res = dbexec_result("select ip from filtering_ip where is_block = 0 and in_use = 1");
	if (NULL == res) {
		syslog(LOG_DEBUG,"is_allowed_src_IP() : db fail");
		return NOT_ALLOW;
	}
	ip_cnt = PQntuples(res);
	if (0 == ip_cnt) {
		syslog(LOG_DEBUG,"is_allowed_src_IP() : there is no ip list");
		return NOT_ALLOW;
	}
	
	for (i = 0; i <ip_cnt; i++) {
		if (src_ip == inet_addr(PQgetvalue(res, i, 0))) {
			syslog(LOG_DEBUG,"is_allowed_IP() : allow ip match : %s", PQgetvalue(res, i, 0));
			return ALLOW;
		}
	}
	
	return NOT_ALLOW;
}

/* [sclee][130625] check if it is allowed domain */
int is_allowed_domain(char *buf, int size)
{
	PGresult *res;
	int domain_cnt;
	int k;
	struct buf *word, msg;
	char *vhost;
	int i, j;

	buf_init(&msg, buf, size);

	if ((i = buf_index(&msg, "\r\nHost: ", 8)) > 0) {
		buf_skip(&msg, i + 8);
		word = buf_tok(&msg, "\r\n", 2);
		vhost = buf_strdup(word);
	} else {
		i = buf_index(&msg, " http://", 8);

		if (i < 0 || i > 8) {
			errx(1, "no virtual host in request");
		}
		buf_skip(&msg, i + 8);
		word = buf_tok(&msg, "/", 1);
		vhost = buf_strdup(word);
	}

	for (j = 0; j < strlen(vhost); j++) {
		if (vhost[j] == ':') {
			vhost[j] = '\0';
			break;
		}
	}


	res = dbexec_result("select domain from filtering_board_allow_domain where in_use = 1");
	if (NULL == res) {
		syslog(LOG_DEBUG, "is_allowed_domain() : db fail");
		return NOT_ALLOW;
	}
	domain_cnt = PQntuples(res);
	if (0 == domain_cnt) {
		syslog(LOG_DEBUG, "is_allowed_domain() : there is no domain list");
		return NOT_ALLOW;
	}

	for (k = 0; k < domain_cnt; k++) {
		if (!strcmp(vhost, PQgetvalue(res, k, 0))) {
			syslog(LOG_DEBUG, "is_allowed_domain() : allow domain match : %s", PQgetvalue(res, k, 0));
			return ALLOW;
		}
	}
	
	return NOT_ALLOW;
}

static int send2loader(uint32_t size, void *body, void *httpss)
{
    struct sockaddr_in logger_addr;
    struct _http *https;

    int to_send_size = 0;
    int sent_size = 0;

    int which_loader = 0;

    syslog(LOG_DEBUG, "in send2loader, size = [%d]", size);

    if (size <= 0 || NULL == body)
        return 0;

    https = (struct _http *) httpss;

    logger_addr.sin_family = AF_INET;
    logger_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

    which_loader = https->http_id % MAX_LOADER;
    switch (which_loader) {
    case 0:
        logger_addr.sin_port = htons(LOADER0_PORT);
        break;

    case 1:
        logger_addr.sin_port = htons(LOADER1_PORT);
        break;

    case 2:
        logger_addr.sin_port = htons(LOADER2_PORT);
        break;

    case 3:
        logger_addr.sin_port = htons(LOADER3_PORT);
        break;
#ifdef __64BIT_LOADER__
    case 4:
        logger_addr.sin_port = htons(LOADER4_PORT);
        break;

    case 5:
        logger_addr.sin_port = htons(LOADER5_PORT);
        break;

    case 6:
        logger_addr.sin_port = htons(LOADER6_PORT);
        break;

    case 7:
        logger_addr.sin_port = htons(LOADER7_PORT);
        break;

    case 8:
        logger_addr.sin_port = htons(LOADER8_PORT);
        break;

    default:
        logger_addr.sin_port = htons(LOADER9_PORT);
#else
	default:
		logger_addr.sin_port = htons(LOADER4_PORT);
#endif
    }

    syslog(LOG_DEBUG, "loader_port=[%d] https->http_id=[%d] ", 
			ntohs(logger_addr.sin_port), https->http_id);

    while (sent_size < size) {
		// XXX increment src address
        to_send_size =
            sendto(socket_to_loader, body, size, 0,
                   (struct sockaddr *) &logger_addr, sizeof(logger_addr));
        sent_size += to_send_size;
    }

    return (sent_size);
}

u_int64_t get_http_log_id(u_int32_t low_id)
{
    u_int64_t http_id;
	http_id = *hi_id;
    http_id = http_id << 32;
    http_id = http_id | low_id;
    http_id = http_id << 4;
    http_id = http_id | *pv_id;

    return (http_id);
}

char *id2name(u_int32_t id)
{
    char *fname;
    u_int64_t id_64 = 0;

    fname = (char *) malloc(sizeof(char) * 65);
    if (fname == NULL) {
        syslog(LOG_WARNING, "malloc failed!\n");
        return NULL;
    }

    id_64 = get_http_log_id(id);
#ifdef __64BIT_LOADER__
    syslog(LOG_DEBUG, "id64 = [%lu]", id_64);
    sprintf(fname, "%s%lu", WREQUEST_BASE_DIR, id_64);
#else
	syslog(LOG_DEBUG, "id64 = [%llu]", id_64);
	sprintf(fname, "%s%llu", WREQUEST_BASE_DIR, id_64);
#endif

    return (fname);
}

int file_write(const char *filename, unsigned int len, const char* content)
{
    FILE *f = fopen(filename, "w");

#ifdef DUMP_REQUEST
    syslog(LOG_DEBUG, "in file_write \n --> content = [%s]", content);
#endif
    fwrite(content, len, 1, f);

    fclose(f);

    return 0;
}

int write_child_pid(const char *filename)
{
    FILE *f;

    if ((f = fopen(filename, "w")) == NULL) {
        syslog(LOG_WARNING, "fopen failed");
        return -1;
    }
    fprintf(f, "%d", (int) getpid());
    fclose(f);
    return 0;
}

int pid_write(u_int32_t id)
{
    char fname[65];
    u_int64_t id_64 = 0;

    id_64 = get_http_log_id(id);
#ifdef __64BIT_LOADER__
    snprintf(fname, 64, "%s%lu", WREQUEST_INSPECTION, id_64);
#else
	snprintf(fname, 64, "%s%llu", WREQUEST_INSPECTION, id_64);
#endif

    if (write_child_pid(fname) < 0)
        return -1;

    return 0;
}

int pid_remove(u_int32_t id)
{
    char fname[65];
    u_int64_t id_64 = 0;

    id_64 = get_http_log_id(id);
#ifdef __64BIT_LOADER__
    snprintf(fname, 64, "%s%lu", WREQUEST_INSPECTION, id_64);
#else
	snprintf(fname, 64, "%s%llu", WREQUEST_INSPECTION, id_64);
#endif

    if (unlink(fname) != 0) {
#ifdef __64BIT_LOADER__
        syslog(LOG_WARNING, 
				"unlink( proxy child pid ) failed : id_64=[%lu]",
                  id_64);
#else
		syslog(LOG_WARNING,
				"unlink( proxy child pid ) failed : id_64=[%llu]",
				id_64);
#endif
        return -1;
    }

    return 0;
}

struct in_addr *name_resolve(char *name)
{
    struct hostent *he;
    struct in_addr **addr_list;
    if ((he = gethostbyname(name)) == NULL)
        return 0;
    addr_list = (struct in_addr **) he->h_addr_list;
    return addr_list[0];
}

static void server_ip_and_port_init(char *buf, int size)
{
    struct buf *word, msg;
    char *vhost;
    int i, j;

    memset(&serverin, 0, sizeof(serverin));
    serverin.sin_family = AF_INET;
    serverin.sin_port = htons(mitm_port);

    if (static_host == 0) {
        buf_init(&msg, buf, size);

        if ((i = buf_index(&msg, "\r\nHost: ", 8)) > 0) {
            buf_skip(&msg, i + 8);
            word = buf_tok(&msg, "\r\n", 2);
            vhost = buf_strdup(word);
        } else {
            i = buf_index(&msg, " http://", 8);

            if (i < 0 || i > 8) {
                errx(1, "no virtual host in request");
            }
            buf_skip(&msg, i + 8);
            word = buf_tok(&msg, "/", 1);
            vhost = buf_strdup(word);
        }
        for (j = 0; j < strlen(vhost); j++) {
            if (vhost[j] == ':') {
                vhost[j] = '\0';
                break;
            }
        }

        struct in_addr *a = name_resolve(vhost);
        if (!a)
            errx(1,
                 "server ip_port_init - couldn't resolve host in request");
        serverin.sin_addr = *a;

        free(vhost);

        if (serverin.sin_addr.s_addr == ntohl(INADDR_LOOPBACK) ||
            serverin.sin_addr.s_addr == -1) {
            errx(1,
                 "server ip_port_init - couldn't resolve host in request");
        }
    } else
        serverin.sin_addr.s_addr = static_host;
}

void to_loader__https(char *buf, int size, int pass, struct _http *https)
{
    void *content;
    char *id_file_name;
    int k;

    loader_response = 0;
    proxy_timeout = 0;

    memset(https, 0, sizeof(struct _http));

    https->tag = TAG__HTTPS;
    https->method = check_method(buf);

    if (https->method != POST) {
        syslog(LOG_DEBUG, "HTTPs LOG : Request method is not POST!");
        return;
    }

    content = &(https->stream[0]);
    https->transfer_type = HTTP_STREAM;
    https->content_size = size;

    https->pass = pass;

    syslog(LOG_DEBUG,
           "POST packet id = [%" PRIu32 "] is entered, Processing Start!",
           https->http_id);

    server_ip_and_port_init(buf, size);

//[sclee][130425] allowed URLs and allowed IPs
        if(dbinit(db_port)) {
               	if ( is_web_server_ip_not_filtering(serverin.sin_addr.s_addr) ) {
			return;
		}
		if ( is_web_server_ip(serverin.sin_addr.s_addr) ) {
			return;
		}
		if ( is_allowed_src_IP(clientin.sin_addr.s_addr) ) {
                        return;
                }
		if ( is_allowed_URL(buf) ) {
                        return;
                }
                if ( is_allowed_domain(buf, size)) {
                        return;
                }
                dbclose();
        }


    https->src_ip = clientin.sin_addr.s_addr;
    https->dst_ip = serverin.sin_addr.s_addr;
    https->src_port = clientin.sin_port;
    https->dst_port = serverin.sin_port;

    https->cnt = 0;

    https->http_id = get__http_id();
    https->pstore_id = 0;

    syslog(LOG_DEBUG, "Client IP = [%s]", inet_ntoa(clientin.sin_addr));

    if (pid_write(https->http_id) < 0) {
        syslog(LOG_WARNING, "pid_write failed");
        return;
    }
    // post 크기가 MAX_UDP_SIZE 보다 크면 content에 파일 이름을, 적으면 내용을 복사한다.
#if 1 /* sclee */
	syslog(LOG_DEBUG,
			"Content size is larger than MAX_UDP_SIZE. Write post to file");

	if ((id_file_name = id2name(https->http_id)) == NULL) {
		syslog(LOG_WARNING, "Get filename failed!\n");
		return;
	}

	file_write(id_file_name, https->content_size, buf);

	struct _message *msg;
	msg = malloc(sizeof(struct _message) + 1024);
    if (NULL == msg) {
        printf("msg malloc failed\n");
        return ;
    }
	msg->magic_num = 1;
    msg->message_size = 2;
    msg->request_id = 1;
    msg->flag = 1;
	msg->file_size = https->content_size;
	msg->file_path_size = strlen(id_file_name);
	memcpy(msg->file_path, id_file_name, msg->file_path_size);
	memcpy(msg->file_path + msg->file_path_size, "\0", 1);
	syslog(LOG_DEBUG, "hihi\n");
    syslog(LOG_DEBUG, "File name : [%s]\n", (char *) content);
    syslog(LOG_DEBUG, "hoho\n");
	

//	https->filename_len = strlen(id_file_name);
//	https->transfer_type = HTTP_FILE;
//	memcpy(content, id_file_name, https->filename_len);
//	memcpy(content + https->filename_len, "\0", 1);
//	syslog(LOG_DEBUG, "File name : [%s]\n", (char *) content);

	free((void *) id_file_name);
	k = send2loader(sizeof(struct _message) + 1024, msg, https);

#else	
    if (https->content_size > MAX_UDP_SIZE) {
        syslog(LOG_DEBUG,
               "Content size is larger than MAX_UDP_SIZE. Write post to file");

        if ((id_file_name = id2name(https->http_id)) == NULL) {
            syslog(LOG_WARNING, "Get filename failed!\n");
            return;
        }

        file_write(id_file_name, https->content_size, buf);

        https->filename_len = strlen(id_file_name);
        https->transfer_type = HTTP_FILE;
        memcpy(content, id_file_name, https->filename_len);
        memcpy(content + https->filename_len, "\0", 1);
        free((void *) id_file_name);
        k = send2loader(LOG__HTTP_SIZE + https->filename_len, https);
    } else {
        memcpy(content, buf, size);
        k = send2loader(LOG__HTTP_SIZE + https->content_size, https);
    }
#endif
    syslog(LOG_DEBUG, "send k : [%d] bytes to loader", k);

    https_id64 = get_http_log_id(https->http_id);
    signal(SIGALRM, sig_proxy_timeout);
#if 0 /* sclee */
    alarm(PROXY_TIMEOUT);
    while ((proxy_timeout == 0) && (loader_response == 0)) {
        sleep(PROXY_TIMEOUT);
    }

    if (proxy_timeout) {
        syslog(LOG_WARNING, "Proxy Timeout");
    }
    if (loader_response) {
        syslog(LOG_DEBUG, "Loader Response");
    }
#endif
    if (https_request_pass == PASS) {
#ifdef __64BIT_LOADER__
        syslog(LOG_DEBUG,
               "HTTPs POST http_id = [%" PRIu32 "], id64 = [%lu], PASS!!",
               https->http_id, https_id64);
#else
		syslog(LOG_DEBUG,
				"HTTPs POST http_id = [%" PRIu32 "], id64 = [%llu], PASS!!",
				https->http_id, https_id64);
#endif
        child_close = 0;
    } else {
#ifdef __64BIT_LOADER__
        syslog(LOG_DEBUG,
               "HTTPs POST http_id = [%" PRIu32
               "], id64 = [%lu], BLOCK!!", https->http_id, https_id64);
#else
		syslog(LOG_DEBUG,
				"HTTPs POST http_id = [%" PRIu32
				"], id64 = [%llu], BLOCK!!", https->http_id, https_id64);
#endif
        child_close = 1;
    }

    if (pid_remove(https->http_id) < 0) {
        syslog(LOG_WARNING, "pid_remove failed");
        return;
    }
    return;
}

static void usage(void)
{
    fprintf(stderr, "Version: " VERSION "\n"
            "Usage: webmitm [-d] [host]\n");
    exit(1);
}

static void err_ssl(int eval, char *msg)
{
    errx(eval, "%s", msg);
}

static void cert_init(void)
{
    struct stat sb;
    char cmd[1024] = { 0x00, };

    if (stat(cert_file, &sb) < 0) {
        sprintf(cmd, "openssl genrsa -out %s 1024", cert_file);
        if (system(cmd) != 0)
            err(1, "system");

        sprintf(cmd, "openssl req -new -key %s -out %s.csr", cert_file,
                cert_file);
        if (system(cmd) != 0)
            err(1, "system");

        sprintf(cmd,
                "openssl x509 -req -days 365 -in %s.csr -signkey %s -out %s.new",
                cert_file, cert_file, cert_file);
        if (system(cmd))
            err(1, "system");

        sprintf(cmd, "cat %s.new >> %s", cert_file, cert_file);
        if (system(cmd) != 0)
            err(1, "system");
        sprintf(cmd, "%s.new", cert_file);
        unlink(cmd);
        sprintf(cmd, "%s.csr", cert_file);
        unlink(cmd);

        syslog(LOG_DEBUG, "certificate generated");
    }
}

static void client_init(void)
{
    if (fcntl(client_fd, F_SETFL, 0) < 0)
        err(1, "fcntl");
#if SSL
    ssl_client = SSL_new(ssl_client_ctx);
    SSL_set_fd(ssl_client, client_fd);

    if (SSL_accept(ssl_client) <= 0) {
        err_ssl(1, "SSL_accept");
    }
#else

#endif
}

static int client_read(char *buf, int size)
{
#if SSL
    return (SSL_read(ssl_client, buf, size));
#else
	return (read(client_fd, buf, size));
#endif
}

int remained = 0;
static int client_request(char *buf, int size, struct _http *https)
{
    struct buf *b, req;
    char *p;
    int i, j, reqlen, content_length=0;
    int content_cur_len = 0;

    memset(&req, 0, sizeof(req));
    req.base = buf;
    req.size = size;
    reqlen = 0;

    alarm(READ_TIMEOUT);
    while ((j = client_read(req.base + req.end, req.size - req.end)) > 0) {
        req.end += j;
		// makes buf null terminated string
		// this is required instead of memset()
		buf[MIN(req.end, max_packet_size)] = 0; 

        if (reqlen && buf_len(&req) >= reqlen) {
            remained = 0;
            break;
        } else if (!content_length) {
            if (!remained && (i = buf_index(&req, "\r\n\r\n", 4)) > 0) {
                reqlen = i + 4;
                b = buf_tok(&req, NULL, reqlen);
                buf_rewind(&req);

                if ((i = buf_index_case(b, "\r\nContent-Length: ", 18)) < 0)
                    break;
                buf_skip(b, i + 18);
                b = buf_getword(b, "\r\n", 2);
                p = buf_strdup(b);
                buf_free(b);
                content_length = atoi(p);

                // sclee
                // if client_read() stopped at header end
                if (j - reqlen < 3)
                    j = 0;

#if 1 /* sclee - passing when content size is big */
		if (content_length > max_packet_size - reqlen) {
		    server_init(buf, req.end);
		    add_x_forward_for(buf, req.end);
		    content_cur_len = j;
		    
		    while ((j = client_read(buf, max_packet_size)) > 0) {
		    	content_cur_len += j;
			buf[MIN(j, max_packet_size)] = 0;
			add_x_forward_for(buf, j);
			
			if (content_cur_len >= content_length)
			    break;
		    }

		    child_close = 0;
		    already_sent = 1;
		    return 1;
		}
#endif

                reqlen += atoi(p);
                remained = atoi(p);
                syslog(LOG_DEBUG, "Content-Length:%d", atoi(p));
                free(p);
            }
        }
        if (reqlen == req.end) {
            remained = 0;
            break;
        }
        if (remained) {
            remained = remained-j;
            if (remained<0)
                break;
        }
    }
	if (nonblock)	
		if (j < 0)
			return j;

    alarm(0);
    reqlen = buf_len(&req);
    syslog(LOG_DEBUG, "reqlen = [%d]", reqlen);
#ifdef DUMP_REQUEST
    if (reqlen)
        syslog(LOG_DEBUG, "%s", req.base);
#endif
    if (reqlen == 0) {
         // When a TCP connection is closed on one side 
         // read() on the other side returns 0 byte.
        return 0;
    } else if (reqlen < max_packet_size) {
        to_loader__https(buf, reqlen, 0, https);
        return (reqlen);
    } else {
        syslog(LOG_WARNING,
               "Request is larger than MAX_BUF_SIZE. Pass");
        child_close = 0;
        return (reqlen);
    }
}

static int client_write(char *buf, int size)
{
#if SSL
    return (SSL_write(ssl_client, buf, size));
#else
	return (write(client_fd, buf, size));
#endif
}

static void client_close(void)
{
#if SSL
	SSL_shutdown(ssl_client); // for ab(apachebench)
    SSL_free(ssl_client);
#endif
    close(client_fd);
}

static void server_init(char *buf, int size)
{
    struct buf *word, msg;
    char *vhost;
    int i, j;
    memset(&serverin, 0, sizeof(serverin));
    serverin.sin_family = AF_INET;
    serverin.sin_port = htons(server_port);

    if (static_host == 0) {
        buf_init(&msg, buf, size);

        if ((i = buf_index(&msg, "\r\nHost: ", 8)) > 0) {
            buf_skip(&msg, i + 8);
            word = buf_tok(&msg, "\r\n", 2);
            vhost = buf_strdup(word);
        } else {
            i = buf_index(&msg, " http://", 8);

            if (i < 0 || i > 8) {
                errx(1, "no virtual host in request");
            }
            buf_skip(&msg, i + 8);
            word = buf_tok(&msg, "/", 1);
            vhost = buf_strdup(word);
        }
        for (j = 0; j < strlen(vhost); j++) {
            if (vhost[j] == ':') {
                vhost[j] = '\0';
                break;
            }
        }
        syslog(LOG_DEBUG, "vhost : %s", vhost);
        struct in_addr *a = name_resolve(vhost);
		if (!a)
            errx(1, "server_init - couldn't resolve host in request");
        serverin.sin_addr = *a;

        free(vhost);

        if (serverin.sin_addr.s_addr == ntohl(INADDR_LOOPBACK) ||
            serverin.sin_addr.s_addr == -1) {
            errx(1, "server_init - couldn't resolve host in request");
        }
    } else
        serverin.sin_addr.s_addr = static_host;


    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        err(1, "socket");

    if (transparent) {
        struct sockaddr_in *bind_addr;
	struct sockaddr sa;
	socklen_t size;
	int one = 1;
	int rc;

	setsockopt(client_fd, SOL_SOCKET, SO_REUSEADDR, (char *)&one, sizeof(int));
	setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, (char *)&one, sizeof(int));

	rc = setsockopt(server_fd, SOL_IP, IP_TRANSPARENT, (char *)&one, sizeof(one));

	size = sizeof(sa);
	rc = getpeername(client_fd, &sa, &size);
	bind_addr = (struct sockaddr_in *)&sa;
	rc = bind(server_fd, (struct sockaddr *)bind_addr, sizeof(struct sockaddr_in));
    }

    if (connect(server_fd, (struct sockaddr *) &serverin, sizeof(serverin))
        < 0)
        err(1, "connect");
#if SSL
	ssl_server_ctx = SSL_CTX_new(SSLv23_client_method());
    ssl_server = SSL_new(ssl_server_ctx);
    SSL_set_connect_state(ssl_server);

    SSL_set_fd(ssl_server, server_fd);

    if (SSL_connect(ssl_server) < 0)
        err_ssl(1, "SSL_connect");
#endif
}

static int server_read(char *buf, int size)
{
#if SSL
    return (SSL_read(ssl_server, buf, size));
#else
	return (read(server_fd, buf, size));
#endif
}

static int server_write(char *buf, int size)
{
#if SSL
    return (SSL_write(ssl_server, buf, size));
#else
	return (write(server_fd, buf, size));
#endif
}

static void server_close(void)
{
#if SSL
    SSL_shutdown(ssl_server);
    SSL_free(ssl_server);
#endif
    close(server_fd);
}

static void mitm_init(void)
{
    struct sockaddr_in sin;
    int i = 1;

    if ((https_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        err(1, "socket");
    if (transparent) {
        setsockopt(https_fd, IPPROTO_TCP, TCP_NODELAY, &i, sizeof(i));
	if (setsockopt(https_fd, SOL_IP, IP_TRANSPARENT, &i, sizeof(int)) == -1) {
	    err(1, "setsockopt");
	}
    } else {
        if (setsockopt(https_fd, SOL_SOCKET, SO_REUSEADDR, &i, sizeof(i)) < 0)
            err(1, "setsockopt");
    }

    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = INADDR_ANY;
    // XXX
    // SO_REUSEADDR does not support INADDR_ANY
    // sin.sin_addr.s_addr = inet_addr(WEBMITM_IP);

    sin.sin_port = htons(mitm_port);
    if (bind(https_fd, (struct sockaddr *) &sin, sizeof(sin)) < 0)
        err(1, "bind");

    if (listen(https_fd, 100) < 0)
        err(1, "listen");
#if SSL
    SSL_library_init();
#ifdef MEMORY_USAGE_ISSUE
    SSL_load_error_strings();
#endif

    ssl_client_ctx = SSL_CTX_new(SSLv23_server_method());

    // if (SSL_CTX_use_certificate_file(ssl_client_ctx, CERT_FILE,
    //     SSL_FILETYPE_PEM) == 0)
    // err_ssl(1, "SSL_CTX_use_certificate_file");

    if (SSL_CTX_use_certificate_chain_file(ssl_client_ctx, cert_file) == 0)
        err_ssl(1, "SSL_CTX_use_certificate_chain_file");

    if (SSL_CTX_use_PrivateKey_file(ssl_client_ctx, cert_file,
                                    SSL_FILETYPE_PEM) == 0)
        err_ssl(1, "SSL_CTX_use_PrivateKey_file");

    if (SSL_CTX_check_private_key(ssl_client_ctx) == 0)
        err_ssl(1, "SSL_CTX_check_private_key");
#endif
}

void add_x_forward_for(char *buf, int len)
{
    char *j;
    int icount = 0;
    char *packet;
	
    char org_ip[MAX_IP_SIZE];

    if (forwarded && ((!strncmp(buf, "POST", 4)) || (!strncmp(buf, "GET", 3)))) {
        if (strstr(buf, "X-Forwarded-For:") == NULL) {
            if ((j = strstr(buf, "Host: ")) != NULL) {
                sprintf(org_ip, "X-Forwarded-For:%s\r\n",
                        inet_ntoa(clientin.sin_addr));
				int len_org_ip = strlen(org_ip);
	            packet = malloc(sizeof(char)*(len+len_org_ip));
                icount = j - buf;
                memcpy(packet, buf, icount);
                memcpy(packet + icount, org_ip, len_org_ip);
                memcpy(packet + icount + len_org_ip,
                       buf + icount, len - icount);
				syslog(LOG_DEBUG, "X-Forwarded-For:");

                if (server_write(packet, len + len_org_ip) !=
                    len + len_org_ip)
                    err(1, "server_write");
	            free(packet);
            } else {
                syslog(LOG_WARNING,
                       "There is no host in packet header");
#ifdef DUMP_REQUEST
                syslog(LOG_DEBUG, "%s", buf);
#endif
                if (server_write(buf, len) != len)
                    err(1, "server_write");
            }
        } else {
            syslog(LOG_WARNING,
                   "there is already forwarded header packet");
#ifdef DUMP_REQUEST
            syslog(LOG_DEBUG, "%s", buf);
#endif
            if (server_write(buf, len) != len)
                err(1, "server_write");
        }
    } else {
        if (server_write(buf, len) != len)
            err(1, "server_write");
    }
}

static void mitm_child(void)
{
	char *buf = malloc(sizeof(char)*max_packet_size + 1); // +1 for sentinel
	int sizeof_buf = max_packet_size;
    fd_set fds;
    int i;
    static struct _http *https;
    int notice_msg_len;

    syslog(LOG_DEBUG, "mitm_child start");

    signal(SIGUSR2, sig_loader_process_finish);

    signal(SIGALRM, sig_giveup);
    alarm(10);
    client_init();
    alarm(0);
    syslog(LOG_DEBUG, "new connection from %s.%d",
              inet_ntoa(clientin.sin_addr), ntohs(clientin.sin_port));


    //signal(SIGALRM, sig_proxy_timeout);
    https = alloc_https_buf();
    if ((i = client_request(buf, sizeof_buf, https)) <= 0) {
//        err(1, "client_request");
	free_https_buf(https);
	client_close();
	return;
    }
    
    if (!already_sent) {
        syslog(LOG_DEBUG, "%d bytes from %s", 
                       i, inet_ntoa(clientin.sin_addr));
#ifdef DUMP_REQUEST
        syslog(LOG_DEBUG, "%s", buf);
#endif

        // POST 가 Block 된 경우.
        if (child_close) {
            notice_msg_len = make_notice(buf);
            client_write(buf, notice_msg_len);
            free_https_buf(https);
            client_close();
            return;
        }
        // 개인정보가 없어 POST Pass이 된 경우 
        server_init(buf, i);
        syslog(LOG_DEBUG, "server inited");

        add_x_forward_for(buf, i);
        syslog(LOG_DEBUG, "add_x_forwared");
    }

    for (;;) {
        FD_ZERO(&fds);
        FD_SET(client_fd, &fds);
        FD_SET(server_fd, &fds);

        i = MAX(client_fd, server_fd) + 1;
        if (select(i, &fds, 0, 0, 0) < 0) {
            if (errno != EINTR) {
                break;
            }
        }
		if (FD_ISSET(client_fd, &fds)) {
			i = sizeof_buf;
			if (nonblock) {
				int flag;
				flag = fcntl(client_fd, F_GETFL, 0);
				fcntl(client_fd, F_SETFL, flag | O_NONBLOCK);

                if ((i = client_request(buf, i, https)) < 0) 
					syslog(LOG_DEBUG, "client_request() return -1");
				else if (i == 0) {
					syslog(LOG_DEBUG, "client_request() return 0");
					break;
				} else {

					syslog(LOG_DEBUG, "%d bytes from %s",
							i, inet_ntoa(clientin.sin_addr));
#ifdef DUMP_REQUEST
					syslog(LOG_DEBUG, "%s", buf);
#endif
					// POST Block 인 경우
					if (child_close == 1) {
						notice_msg_len = make_notice(buf);
						client_write(buf, notice_msg_len);
						break;
					}
					add_x_forward_for(buf, i);
				}
			} else {
				if ((i = client_request(buf, i, https)) <= 0)
					break;

				syslog(LOG_DEBUG, "%d bytes from %s", 
						i, inet_ntoa(clientin.sin_addr));
#ifdef DUMP_REQUEST
				syslog(LOG_DEBUG, "%s", buf);
#endif
				// POST Block 인 경우
				if (child_close == 1) {
					notice_msg_len = make_notice(buf);
					client_write(buf, notice_msg_len);
					break;
				}
				add_x_forward_for(buf, i);
			}
		}
        if (FD_ISSET(server_fd, &fds)) {
            syslog(LOG_DEBUG, "read a server data");
            i = sizeof_buf;
            if ((i = server_read(buf, i)) <= 0)
                break;
            syslog(LOG_DEBUG, "%d bytes from %s", i, inet_ntoa(serverin.sin_addr));
#ifdef DUMP_REQUEST
            syslog(LOG_DEBUG, "%s", buf);
#endif
            if (client_write(buf, i) != i)
                break;
        }
    }
    server_close();
    client_close();

    free_https_buf(https);
	free(buf);
}

static void mitm_run(void)
{
    fd_set fds;
    socklen_t addrlen;

    struct sigaction act;
    sigset_t mask, orig_mask;

    memset(&act, 0, sizeof(act));
    act.sa_handler = sig_chld;
    if (sigaction(SIGCHLD, &act, 0)) {
        err(1, "sigaction SIGCHLD");
    }

    memset(&act, 0, sizeof(act));
    act.sa_handler = sig_int;
    if (sigaction(SIGINT, &act, 0)) {
        err(1, "sigaction SIGINT");
    }

    sigemptyset(&mask);
    sigaddset(&mask, SIGCHLD);
    sigaddset(&mask, SIGINT);
    if (sigprocmask(SIG_BLOCK, &mask, &orig_mask) < 0) {
        err(1, "sigprocmask SIGCHLD");
    }

    /* notice file init */
    notice = malloc(sizeof(char) * MAX_NOTICE_SIZE);
    read_notice_file();
    syslog(LOG_DEBUG, "Read Notice File Success!");

    if (fcntl(https_fd, F_SETFL, O_NONBLOCK) < 0)
        err(1, "fcntl");

    for (; !exit_request;) {
        //printf("# procs: %d\n", nchilds);
        FD_ZERO(&fds);
        FD_SET(https_fd, &fds);

        int res = pselect(https_fd + 1, &fds, 0, 0, 0, &orig_mask);
        if (res < 0 && errno != EINTR)
            err(1, "pselect");
        else if (exit_request)
            break;
        else if (res == 0)
            continue;

        addrlen = sizeof(clientin);
        if (FD_ISSET(https_fd, &fds)) {
            client_fd =
                accept(https_fd, (struct sockaddr *) &clientin, &addrlen);
        } else
            errx(1, "select failure");

        if (client_fd < 0) {
            if (errno != EINTR && errno != EWOULDBLOCK)
                err(1, "accept");
            else if (errno == EAGAIN)
                continue;
            else
                err(1, "accept unknown");
        }

        while (nchilds > MAX_WEBMITM_PROC) {
			if (exit_request)
				exit(1);
            pselect(1, 0, 0, 0, 0, &orig_mask);
		}

        nchilds++;

        if (fork() == 0) {
            prctl(PR_SET_PDEATHSIG, SIGHUP, 0, 0, 0);
            if (getppid() == 1) // parent died already?
                kill(getpid(), SIGHUP);
            dbclose();
            close(https_fd);
            mitm_child();
            syslog(LOG_DEBUG, "mitm_child finish");
            exit(0);
        }
        close(client_fd);
    }
    close(https_fd);
    free(notice);
}

static void signal_loader_started(int sig)
{
    syslog(LOG_NOTICE, "SIGUSR1 caught. Loader is started. Webmitm will hi_id update. ");
    *hi_id = get__hi_id(*pv_id);
    syslog(LOG_DEBUG, "pv_id = [%d] hi_id = [%d]", *pv_id, *hi_id);
}

int read_pass_or_block(const char *filename)
{
    FILE *f;
    int pass_tmp = 1;

    if ((f = fopen(filename, "r")) == NULL) {
        syslog(LOG_DEBUG, "fopen failed");
        return -1;
    }
    fscanf(f, "%d", &pass_tmp);
    fclose(f);
    syslog(LOG_DEBUG, "read_pass_or_block pass=[%d]", pass_tmp);
    return pass_tmp;
}

static void write_pid(void)
{
    FILE *f = fopen("/usr/local/privacycenter/etc/pidof_webmitm", "w");

    fprintf(f, "%d", (int) getpid());

    fclose(f);
}

int main(int argc, char *argv[])
{
    extern char *optarg;
    extern int optind;
    int c;

    int shm_hi_http_id = 0;
    int pv_id_temp = 1;
    strcpy(db_port, "5432");
    strcpy(cert_file, "/usr/local/privacycenter/etc/web.pem");

	openlog("PV", 0, LOG_LOCAL0);

    while ((c = getopt(argc, argv, "i:p:s:w:m:f:xtndh?V")) != -1) {
        switch (c) {
        case 'd':
            log_level++;
            break;
        case 'i':
            pv_id_temp = atoi(optarg);
            break;
        case 'p':
            if (strlen(optarg) <= 5) {
                memset(db_port, 0x00, 6 * sizeof(char));
                strcpy(db_port, optarg);
            }
            break;
        case 'w':
            if (strlen(optarg) <= 5) {
                mitm_port = atoi(optarg);
                server_port = mitm_port;
            }
            break;
	case 'm':
	    if (strlen(optarg) <= 3) {
	    	max_packet_size = atoi(optarg) * 1024 * 1024;
	    }
	    break;
        case 'f':
            if (strlen(optarg) <= 254) {
                memset(cert_file, 0x00, 256 * sizeof(char));
                strcpy(cert_file, optarg);
            }
            break;
        case 's':
            if (strlen(optarg) <= 5) {
                server_port = atoi(optarg);
            }
            break;
        case 'x':
            syslog(LOG_NOTICE, "X-Forwarded-For header enabled");
            forwarded = 1;
            break;
	case 't':
	    transparent = 1;
	    break;
	case 'n':
		nonblock = 1;
		break;
        default:
            usage();
        }
    }

    argc -= optind;
    argv += optind;

    if (argc == 1) {
		struct in_addr *a = name_resolve(argv[0]);
        if (!a)
            usage();
        static_host = (*a).s_addr;
    } else if (argc != 0)
        usage();

	setlogmask(LOG_UPTO(log_level));

    if (!dbinit(db_port)) {
        syslog(LOG_WARNING, "dbinit() failed");
        return -1;
    }

    if (!dbconnok()) {
        syslog(LOG_WARNING, "dbconnok() failed");
        return -1;
    }
#if SSL
    cert_init();
#endif
    mitm_init();

    /* Shared memory create */
    if ((shm_hi_http_id =
         shmget((key_t) SHM_KEY, (size_t) 12, IPC_CREAT | 0666)) == -1) {
        syslog(LOG_WARNING, "shared memory create failed");
        return -1;
    }

    /* shared memory attach */
    if ((_http_id =
         (u_int32_t *) shmat(shm_hi_http_id, NULL, 0)) == (void *) -1) {
        syslog(LOG_WARNING, "shared memory attach failed");
        return -1;
    }

    write_pid();

	struct sigaction act;
	memset(&act, 0, sizeof(act));
	act.sa_handler = signal_loader_started;
	if (sigaction(SIGUSR1, &act, 0))
        err(1, "sigaction SIGUSR1");

    if (sem_init(&http_id_sem, 0, 1) < 0)
        err(2, "sem");

    sem_wait(&http_id_sem);
    /* shared memory init */
    _http_id[0] = 0;
    _http_id[1] = pv_id_temp;
    _http_id[2] = get__hi_id(pv_id_temp);
    pv_id = &_http_id[1];
    hi_id = &_http_id[2];
    sem_post(&http_id_sem);
    syslog(LOG_DEBUG, "pv_id = [%d] hi_id = [%d]", *pv_id, *hi_id);

    if (init__to_loader() < 0)
        return -1;

    syslog(LOG_DEBUG, "socket create success");

    if (static_host == 0) {
        syslog(LOG_NOTICE, "relaying transparently");
    } else {
        syslog(LOG_NOTICE, "relaying to %s", argv[0]);
    }

    mitm_run();

    sem_destroy(&http_id_sem);

    shmdt(_http_id);

    dbclose();

    exit(0);
}
