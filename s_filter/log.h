
#ifndef __LOG_H
#define __LOG_H

#define  TAG_HTTP                 1
#define  TAG_FIELD                2
#define  TAG_FILE                 3
#define  TAG_AFILE                5

#define  TAG__TOO_LARGE           6
#define  TAG__ADD_FILTER          7
#define  TAG__DELETE_FILTER       8
#define  TAG__REFRESH_FILTER      9

#define  TAG__HTTPS               111
#define  TAG__HTTPS_FILE          112


#define  URL_SIZE        512
#define  NAME_SIZE       128

#define HOST_SIZE		128
#define REFERER_SIZE	512

/* Consider VERY_LARGE_BUF_LEN in common.h and the maximum limit of text type when adjusting this value. */
#define  VALUE_SIZE    4194304  /*  4 MBytes  */
#define  PATH_SIZE         512

#define  KIND__FIELD               0x02
#define  KIND__FILE                0x04
#define  KIND__BLOCK_FILE          0x05
#define  KIND__BLOCK_FILE_SIZE     0x06
#define  KIND__BLOCK_FILE_EXT      0x07
#define  KIND__BLOCK_SRC_IP        0x08


#define  LOG__BUFFER_SIZE   (1024 * 1024 * 64)  /*  64 MBytes : order 11  */

#define  LOG__HTTP_SIZE               56        /*  4 X 12 + 2 X 2 + 4 */
#define  LOG__HTTP_FIELD_SIZE         12        /*  4 X 3  */
#define  LOG__HTTP_FILE_SIZE          20        /*  4 X 5  */
#define  LOG__BLOCK_FILE_SIZE          8        /*  4 X 2  */
#define  LOG__BLOCK_FILE_SIZE_SIZE    16        /*  4 X 4  */
#define  LOG__BLOCK_FILE_EXT_SIZE      8        /*  4 X 2  */
#define  LOG__BLOCK_SRC_IP_SIZE        8        /*  4 X 2  */
#define  LOG__AFILE_SIZE              16        /*  4 X 4  */

#define  KEYWORD_LEN                 80

#if 1                           /* mswon */
#define HTTP_STREAM					  0
#define HTTP_FILE					  1

#define MAX_UDP_SIZE				  (1024 * 64)
#define MAX_BUF_SIZE				  (1024 * 1024 * 32) /* 32 MBytes */
#define MAX_IP_SIZE					  35
#endif

#define ALLOW 1
#define NOT_ALLOW 0

/* https method */
#define POST	2
#define	OTHERS	1

/* https pass */
#define PASS	1
#define BLOCK	0

#define  PROXY_TIMEOUT	5
#define  READ_TIMEOUT 300

#define  FILE_NAME_LEN                40
#define  AFILE_BASE_DIR      "/usr/local/privacycenter/AttachedFile/"
#define  WREQUEST_BASE_DIR   "/usr/local/privacycenter/HTTP_File/"
#define  WREQUEST_INSPECTION "/usr/local/privacycenter/HTTP_File/inspection/"
#define	 NOTICE_FILE		 "/usr/local/privacycenter/etc/notice.html"
#define  MAX_NOTICE_SIZE            8398        /* MAX_NOTICE_HTML_SIZE + 200(NOTICE_HDR_SIZE) + NOTICE_TAIL_SIZE */

#define PV_LOG_LEVEL LOG_WARNING

struct _http {
    u_int32_t tag;
    u_int32_t http_id;
    u_int32_t pass;
    u_int32_t pstore_id;
    u_int32_t method;
    u_int32_t src_ip;
    u_int32_t dst_ip;
    u_int16_t src_port;
    u_int16_t dst_port;
    u_int32_t url_len;
    u_int32_t referer_len;
    u_int32_t cnt;
    u_int32_t transfer_type;
    u_int32_t content_size;
    u_int32_t filename_len;
    char stream[1];
};

struct _message {
    u_int32_t magic_num;
    u_int32_t message_size;
    u_int32_t request_id;
    u_int32_t flag;
    u_int32_t file_size;
    u_int32_t file_path_size;
    char file_path[1024];
};


struct _tlv {
    u_int32_t kind;
    char data[1];
};

struct _field {
    u_int32_t kind;
    u_int32_t name_len;
    u_int32_t value_len;
    char data[1];
};

struct _file {
    u_int32_t kind;
    u_int32_t type;
    u_int32_t name_len;
    u_int32_t fname_len;
    u_int32_t path_len;
    char data[1];
};

struct _block_file {
    u_int32_t kind;
    u_int32_t path_len;
    char data[1];
};


struct _block_file_size {
    u_int32_t kind;
    u_int32_t file_size;
    u_int32_t block_size;
    u_int32_t path_len;
    char data[1];
};


struct _block_file_ext {
    u_int32_t kind;
    u_int32_t path_len;
    char data[1];
};

struct _block_src_ip {
    u_int32_t kind;
    u_int32_t src_ip;
};


struct _too_large {
    u_int32_t tag;
    u_int32_t http_id;
    u_int32_t pstore_id;
    u_int32_t src_ip;
    u_int32_t dst_ip;
    u_int16_t src_port;
    u_int16_t dst_port;
    u_int32_t url_size;
    char host_name[NAME_SIZE];
    char referer[URL_SIZE];
    char url[1];
};


struct _msg_to_loader {
    u_int32_t tag;
    char data[KEYWORD_LEN];
};


struct _log {
    u_int32_t tag;
    char data[1024 * 1024 * 4];
};

/* Define if you have not memory usage issue */
#define MEMORY_USAGE_ISSUE

#define MAX_WEBMITM_PROC 100 

#endif                          /* __LOG_H */
