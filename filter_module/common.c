#include <stdio.h>
#include <linux/pg.h>
#include <signal.h>
#include <sched.h>
#include <stdlib.h>
#include <asm/types.h>
#include <arpa/inet.h>
//#include <linux/byteorder/generic.h>
#include <string.h>
#include <error.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <syslog.h>
#include <wchar.h>
#include <time.h>

#include "logger.h"
#include "common.h"
//#include "log.h"

#define p_name "filter_module"

/*
 * Fork away from the controlling terminal (-S option)
 */
void daemonize(void)
{
    int i;
    pid_t pid;

    pid = fork();
    if (pid == (pid_t) - 1) {
        fprintf(stderr, "could not fork background process: %s\n",
            strerror(errno));
        exit(0);
    } else if (pid) {   /* parent */
        /* Parent should just exit, without doing any atexit cleanup */
        exit(0);
    }

    i = open("/dev/null", O_RDWR);
    dup2(i, 0);
    dup2(i, 1);
    dup2(i, 2);
    close(i);
}


inline u_char my_tolower(u_char c)
{
    if ('A' <= c && c <= 'Z')
        c -= ('A' - 'a');
    if (14 == c)
        c = 10;
    return (c);
}

inline size_t tolower_text(char *p)
{
    size_t len;
    int i;

    if (NULL == p)
        return (0);

    len = strlen(p);
    for (i = 0; i < len; i++)
        p[i] = my_tolower(p[i]);

    return (len);
}


inline void utoa(unsigned int n, char *buffer, unsigned int buf_len)
{
    int index;
    int temp;

    if (NULL == buffer || 0 == buf_len)
        return;

    buffer[buf_len] = '\0';

    for (index = buf_len - 1; 0 <= index; index--) {
        temp = n % 10;
        switch (temp) {
        case 0:
            buffer[index] = '0';
            break;
        case 1:
            buffer[index] = '1';
            break;
        case 2:
            buffer[index] = '2';
            break;
        case 3:
            buffer[index] = '3';
            break;
        case 4:
            buffer[index] = '4';
            break;
        case 5:
            buffer[index] = '5';
            break;
        case 6:
            buffer[index] = '6';
            break;
        case 7:
            buffer[index] = '7';
            break;
        case 8:
            buffer[index] = '8';
            break;
        case 9:
            buffer[index] = '9';
            break;
        }
        n = n / 10;
    }
}

/*  End of utoa()  */

inline void replaceQuotWithSpace(char *string)
{
    int size;
    int i;

    if (NULL == string || 0 == (size = strlen(string)))
        return;

    for (i = 0; i < size; i++) {
        if (0x27 == *(string + i))  /*  0x27 : single quat.   */
            *(string + i) = ' ';
    }
}

//FIXME: escape string should be done just before query executing. Not to be done by individually.
inline int escapeSingleQuot(char *to, char *from)
{
    int size;
    int i, j;
    int count = 0;

    if (NULL == from || NULL == to)
        return count;

    if (0 == (size = strlen(from))) {
        *to = '\0';
        return count;
    }

    for (i = 0, j = 0; i < size; i++, j++) {
        /*  0x27 is a single Quot.  0x22 is a double Quot. 0x5C is a reverse slash.  */
        if (0x27 == *(from + i) || 0x22 == *(from + i)
            || 0x5c == *(from + i)) {
            *(to + j) = '\\';
            j++;
            count++;
        }

        *(to + j) = *(from + i);
    }
    *(to + j) = '\0';

    return count;
}

/*  End of escapeSingleQuot()  */

inline void trim_url(char *url)
{
    int url_len;
    int i;

    if (NULL == url)
        return;

    url_len = strlen(url);
    for (i = 0; i < url_len; i++)
        if ('?' == *(url + i)) {
            *(url + i) = '\0';
            break;
        }
}

inline int is_digit(char c)
{
    return ('0' <= c && c <= '9');
}

inline int is_not_digit(char c)
{
    return (c < '0' || '9' < c);
}

inline int is_alpha(char c)
{
    return ((c >= 65 && c <= 90) || (c >= 97 && c <= 122));
}

inline int is_wide_char_string(char *field)
{
    int size = strlen(field);
    int i;

    for (i = 0; i < size; i++)
        if (0x80 == (0x80 & *(field + i)))
            return (1);

    return (0);
}

/*  End of is_wide_char_string() */

inline int my_strcmp(const char *s1, const char *s2)
{
    if (NULL != s1 && NULL != s2)
        return (strcmp(s1, s2));

    if (NULL == s1)
        return (-1);
    else
        return (1);
}

/*  End of my_strcmp()  */

inline int my_strcasecmp(const char *s1, const char *s2)
{
    if (NULL != s1 && NULL != s2)
        return (strcasecmp(s1, s2));

    if (NULL == s1)
        return (-1);
    else
        return (1);
}

/*  End of my_strcasecmp()  */

inline int my_strncmp(const char *s1, const char *s2, size_t n)
{
    if (NULL != s1 && NULL != s2)
        return (strncmp(s1, s2, n));

    if (NULL == s1)
        return (-1);
    else
        return (1);
}

/*  End of my_strncmp()  */

inline size_t my_strlen(const char *s)
{
    if (NULL == s)
        return (0);

    return (strlen(s));
}

/*  End of my_strlen()  */

/*
* A faster replacement for inet_ntoa().
*/
char *_intoa(unsigned int addr, char *buf, unsigned int bufLen)
{
    char *cp, *retStr;
    u_int byte;
    int n;

    cp = &buf[bufLen];
    *--cp = '\0';

    n = 4;
    do {
        byte = addr & 0xff;
        *--cp = byte % 10 + '0';
        byte /= 10;
        if (byte > 0) {
            *--cp = byte % 10 + '0';
            byte /= 10;
            if (byte > 0)
                *--cp = byte + '0';
        }
        *--cp = '.';
        addr >>= 8;
    } while (--n > 0);

    /* Convert the string to lowercase */
    retStr = (char *)(cp + 1);

    return (retStr);
}

inline unsigned int trans_ip(unsigned int ip)
{
    unsigned short a, b;
    a = ip >> 16;
    b = ip & 0x0000ffff;
    a = ntohs(a);
    b = ntohs(b);
    return (a | (b << 16));
}

void my_system(char *cmd)
{
    int ret;

    if (NULL == cmd || 0 == my_strlen(cmd)) {
        loaderLog(L_ERR, "my_system() cmd is NULL");
        return;
    }

    ret = system(cmd);

    if (ret < 0)
        loaderLog(L_ERR, "my_system() cmd=[%s] failes", cmd);
    else if (127 == ret)
        loaderLog(L_ERR, "my_system() cmd=[%s] : execve failes", cmd);
}

void get_time( char *buf )
{
    struct tm *ptr;
    time_t lt;

    lt = time(NULL);
    ptr = localtime(&lt);
    strftime(buf, 1024, "%c", ptr);
}

