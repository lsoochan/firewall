#ifndef _DB_H

#define _DB_H       1

//#include <postgresql/libpq-fe.h>
#include <libpq-fe.h>

//#define DB_HOST       "127.0.0.1"
#define DB_NAME "privacycenter"
//#define DB_NAME	"test"
//#define DB_PORT       "5432"

#define NUM_TUPLES( result ) PQntuples( result )

#define DB_STATUS_OK( x ) x >= 0

enum { DB_QUERY_FAIL = -999, DB_CONN_ERR, DB_CONN_FAIL, DB_CONN_OK = 1, DB_QUERY_OK };  // Any failure values must be less than zero.

int dbinit(char *ui_host, char *db_port);
int dbconnok();
int dbstatus();
int dbreset();
void dbclose();

int setstatus(int dbstatno);

inline int dbexec_no_result(const char *sql_fmt, ...);
inline PGresult *dbexec_result(const char *sql_fmt, ...);

#endif
