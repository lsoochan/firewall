#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <pthread.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <syslog.h>
#include <wchar.h>
#include <locale.h>
#include <tre/regex.h>
#include <unistd.h>

//#include "filter_module.h"
#include "logger.h"

#include "http_parsing.h"
#include "privacy_filter.h"

#define p_name "filter_module"

int sock[MAX_FILTER];
int my_sock;

static int keyword_cnt = 0;
static char *keywords_utf8[MAX_KEYWORD_CNT];
static wchar_t *keywords_wide[MAX_KEYWORD_CNT];

static char *patterns[MAX_PATTERN_CNT];
static wchar_t *wpatterns[MAX_PATTERN_CNT];
static regex_t *c_patterns[MAX_PATTERN_CNT];
static int patterns_cnt = 0;


/**
* @author		sclee
* @date			2014-07-04
* @remark		프로세스 내 시그널 핸들링 함수
* @param		sig_num : 시그널 넘버
* @return		없음
*/

static void signal_handler(int sig_num)
{
	signal(sig_num, SIG_DFL);

	switch (sig_num) {
		case SIGALRM:
			add_timeout_log(NULL);
			writeTimeOutLog();
			close(my_sock);
			_exit(0);
			break;
		default:
			loaderLog(L_ERR, "Undefined signal caught.");
			break;
	}


}

/**
* @author		sclee
* @date			2014-07-04
* @remark		필터링 키워드 로딩 함수
* @param		없음
* @return		없음
*/

void keyword_loading()
{
    PGresult *res = NULL;
    char *keyword;
    int keyword_size = 0;
    int cnt;
    int err_no;
    int filtering_grade;
    int i;


    res =
        dbexec_result
        ("select filtering_grade from filtering_basic_option order  by id");
    if (NULL == res) {
        loaderLog(L_ERR, "filter_loading() : sql fails");
        return;
    }
    cnt = PQntuples(res);
    if (cnt < 1) {
        loaderLog(L_INFO, "filter_loading() : no filtering option");
        goto Patterns;
    }
    filtering_grade = atoi(PQgetvalue(res, 0, 0));
    PQclear(res);

    if (1 != filtering_grade && 2 != filtering_grade
            && 3 != filtering_grade)
        filtering_grade = 1;

    res =
        dbexec_result
        ("select keyword from filtering_keyword where in_use = 1 and is_regex = 0 and grade <= %d",
         filtering_grade);

    if (NULL == res) {
        loaderLog(L_ERR, "filter_loading() : sql fails");
        return;
    }

    cnt = PQntuples(res);
    if (cnt < 1) {
        loaderLog(L_INFO, "filter_loading() : no keywords");
        goto Patterns;
    }

    loaderLog(L_INFO, " The number of keywords is [%d] ", cnt);

    if (MAX_KEYWORD_CNT < cnt) {
        loaderLog(L_ERR,
                "filter_loading() : too many keywords : cnt=[%d]",
                cnt);
        cnt = MAX_KEYWORD_CNT;
    }
    for (i = 0; i < cnt; i++) {
        keyword = PQgetvalue(res, i, 0);


        keyword_size = my_strlen(keyword) + 1;
        keywords_utf8[i] = malloc(keyword_size);
        keywords_wide[i] =
             malloc(keyword_size * sizeof(wchar_t) + 1);

        if (NULL == keywords_utf8[i] 
                || NULL == keywords_wide[i]) {
            keyword_cnt = i;
            loaderLog(L_ERR, "filter_loading() : malloc() fails");
            PQclear(res);

            if (NULL != keywords_utf8[i])
                free(keywords_utf8[i]);

            if (NULL != keywords_wide[i])
                free(keywords_wide[i]);

            keywords_utf8[i] = NULL;
            keywords_wide[i] = NULL;

            return;
        }

        strcpy(keywords_utf8[i], keyword);

        if ((size_t) - 1 ==
                mbstowcs(keywords_wide[i], keywords_utf8[i],
                    keyword_size )) {
            keyword_cnt = i;
            loaderLog(L_ERR,
                    "filter_loading() : mbstowcs() fails");
            PQclear(res);

            free(keywords_utf8[i]);
            free(keywords_wide[i]);

            keywords_utf8[i] = NULL;
            keywords_wide[i] = NULL;

            return;
        }else
			loaderLog(L_ERR, "LSC ststs");

    }

#if 1
    {
        int j;
        for( j = 0; j < cnt; j++ )
        {
            loaderLog( L_INFO, "Keyword Filter [%d] : [%s]\n", j + 1, keywords_utf8[j] );
        }
    }
#endif

Patterns:
    keyword_cnt = cnt;

    PQclear(res);

    res =
        dbexec_result
        ("select keyword from filtering_keyword where in_use = 1 and is_regex = 1 ");
    if (NULL == res) {
        loaderLog(L_ERR, "filter_loading() : sql fails");
        return;
    }

    cnt = PQntuples(res);
    if (cnt < 1) {
        loaderLog(L_ERR, "filter_loading() : no patterns");
        PQclear(res);
        return;
    }
    if (MAX_PATTERN_CNT < cnt) {
        loaderLog(L_ERR,
                "filter_loading() : too many patterns : cnt=[%d]",
                cnt);
        cnt = MAX_PATTERN_CNT;
    }

    loaderLog(L_INFO, " The number of patterns is [%d] ", cnt);
    for (i = 0; i < cnt; i++) {
        keyword = PQgetvalue(res, i, 0);

        keyword_size = my_strlen(keyword);
        patterns[i] = malloc(keyword_size + 1);
        wpatterns[i] = malloc(keyword_size * sizeof(wchar_t) + 1);
        if (NULL == patterns[i] || NULL == wpatterns[i]) {
            PQclear(res);

            patterns_cnt = i;
            loaderLog(L_ERR, "filter_loading() : malloc() fails");

            if (NULL == patterns[i])
                free(patterns[i]);

            if (NULL == wpatterns[i])
                free(wpatterns[i]);

            return;
        }

        strcpy(patterns[i], keyword);
        if ((size_t) - 1 ==
                mbstowcs(wpatterns[i], patterns[i], keyword_size)) {
            PQclear(res);

            patterns_cnt = i;
            loaderLog(L_ERR,
                    "filter_loading() : converting pattern = %s to wide character fails",
                    keyword);

            free(patterns[i]);
            free(wpatterns[i]);

            return;
        }

        c_patterns[i] = (regex_t *) malloc(sizeof(regex_t));
        if (NULL == c_patterns[i]) {
            PQclear(res);

            patterns_cnt = i;
            free(wpatterns[i]);
            free(patterns[i]);
            loaderLog(LOG_ERR, "malloc fail \n");
            return;
        }
        memset(c_patterns[i], 0, sizeof(regex_t));

        /*
REG_EXTENDED : Use POSIX Entended Regular Expression syntax.
         */
        err_no = regwcomp(c_patterns[i], wpatterns[i], REG_EXTENDED);
        if (0 != err_no) {
            PQclear(res);

            patterns_cnt = i;
            free(patterns[i]);
            free(wpatterns[i]);
            regfree(c_patterns[i]);
            free(c_patterns[i]);
            loaderLog(L_ERR, "regwcomp() fail \n");
            return;
        }
    }
    patterns_cnt = cnt;

    PQclear(res);
}

/**
* @author		sclee
* @date			2014-07-04
* @remark		필터 refresh 함수
* @param		없음
* @return		없음
*/

void filter_refresh()
{
	keyword_loading();
	init_keywords(keywords_wide, keyword_cnt, c_patterns, patterns_cnt);
}



/**
* @author		sclee
* @date			2014-07-04
* @remark		메인 필터링 함수
* 				각 필드별로 필터링
* @param		http_tlv : 필터링 정보를 가진 구조체
* @return		없음
*/

static void processingHTTP(struct _http_tlv *http_tlv)
{
	int num_fields = 0;
	int num_afiles = 0;
	int value_len;
	struct _tlv *tlv;
	struct _field *field;
	struct _field *field_list[MAX_FIELDS_NUM];
	struct _file *file;
	struct _file *file_list[MAX_AFILES_NUM];
	int i;

	tlv = (struct _tlv *)(http_tlv->data + http_tlv->url_len + http_tlv->referer_len);

	for (i = 0; i < http_tlv->cnt; i++) {
		if (KIND__FIELD == tlv->kind) {
			field = (struct _field *)tlv;
			value_len = my_strlen(field_Value(field));
			
			if (0 != value_len) {
				tolower_fieldName(field);
				loaderLog(L_DEBUG, "field name : %s", field_Name(field));

				field_list[num_fields] = field;
				//
				int flag = 0;
				flag = filter_privacy(field_Value(field), value_len);
				loaderLog(L_DEBUG, "filter_privacy ret : %d", flag);
				wchar_t *w_field;
				int w_len = 0;
				w_field = malloc(value_len * sizeof(wchar_t) + 1);
				if (NULL == w_field) {
					loaderLog(L_ERR, "w_field malloc fail");
					
				}else {
					w_len = mbstowcs(w_field, field_Value(field), value_len * sizeof(wchar_t));
					if ((size_t) - 1 == w_len) {
						loaderLog(L_ERR, "w_field mbstowcs fail");
					}else {
						 loaderLog(L_DEBUG,"keyword : %lu", filter_keyword(w_field));
						 loaderLog(L_DEBUG,"regex : %lu", filter_regex(w_field));
					}
					free(w_field);
				}
				//
				num_fields += 1;
			}
			tlv = (struct _tlv *)( (char *)field + fieldSize(field) );
				
		} else if (KIND__FILE == tlv->kind) {
			loaderLog(L_DEBUG, "it is file");

			file = (struct _file *)tlv;
			file_list[num_afiles] = file;
			num_afiles += 1;
			tlv = (struct _tlv *)( (char *)file + fileSize(file) );
		} else {
			loaderLog(L_DEBUG, "invalid tlv->kind = [%d]", tlv->kind);
			break;
		}
	}


	if (0 < num_afiles) {
		//processing_afiles(num_afiles, file_list);	
	}

}

/**
* @author		sclee
* @date			2014-07-04
* @remark		자식프로세스가 처음 수행하는 함수
*				UDP port 통신 대기
* @param		db_host : 접속할 DB의 IP
*				db_port : 접속할 DB의 PORT
*				my_id : 자식 프로세스 ID
* @return		없음
*/

static void doTheJob(char *db_host, char *db_port, int my_id)
{
	struct _message *msg;
	struct _response resp;
	struct _http_tlv *http_tlv;
	void *stream;
	char *packet;
	int stream_len;
	int sd_current;
	int addrlen;
	struct   sockaddr_in  pin ;

	loaderLog(L_DEBUG, "doTheJob() : start");

	if (!dbinit(db_host, db_port)) {
		loaderLog(L_DEBUG, "doTheJob() : db_init fail");
		exit(0);
	}

	if (dbconnok()) {
		filter_refresh();
	} else {
	}
	
	stream = (void *)malloc(MAX_MSG_SIZE);
	if (NULL == stream) {
		loaderLog(L_ERR, "doTheJob() : stream malloc fail");
		exit(0);
	}

	http_tlv = malloc(MAX_PACKET_SIZE);
	if (NULL == http_tlv) {
        loaderLog(L_ERR, "doTheJob() : http_tlv malloc fail");
        exit(0);
    }
	loaderLog(L_ERR, "doTheJob() : accept start");

	for (;;) {
		if ( ( sd_current = accept(my_sock, (struct sockaddr *)  &pin, &addrlen ) ) == -1 ){
			loaderLog(L_ERR, "doTheJob() : accept failed");
			continue;
		}
		if ((stream_len = recv(sd_current, stream, MAX_MSG_SIZE, 0)) <= 0) {
			loaderLog(L_ERR, "doTheJob() : recv failed : %d", stream_len);
			continue;
		}
		loaderLog(L_DEBUG, "doTheJob() : msg recv!, len : %d", stream_len);
		((char *)stream)[stream_len] = '\0';
		msg = (struct _message *)stream;
		loaderLog(L_DEBUG, "doTheJob() : msg filepath : %s", msg->file_path);
		loaderLog(L_DEBUG, "doTheJob() : msg filepath_len : %d", msg->file_path_size);
		loaderLog(L_DEBUG, "doTheJob() : msg magic : %x", msg->magic_num);
		//if (msg->file_size > MAX_PACKET_SIZE) {
		//	loaderLog(L_ERR, "doTheJob() : file size bigger than MAX_PACKET_SIZE");
			// passing job
		//}	
		packet = read_http_file(msg);

		if (NULL == packet) { // PASS
		}

		http_parsing(packet, http_tlv);
		processingHTTP(http_tlv);
		//

		resp.magic = msg->magic_num;
		resp.rid = msg->request_id;
		resp.flag = 0;
		resp.rcode = 101;
		resp.mlen = 2;
		memcpy(resp.mess, "OK", resp.mlen);
		resp.len = 25 + resp.mlen;

		if (send(sd_current, (void *)&resp, resp.len, 0) == -1) {
			loaderLog(L_DEBUG, "doTheJob() : sending error\n");
		}
		loaderLog(L_DEBUG, "doTheJob() : send finish\n");
		
		free(packet);
	}
}

/**
* @author       sclee
* @date         2014-07-04
* @remark       자식프로세스 생성 함수
* @param        child_id : 자식 프로세스 ID
*				filter_child_port : 자식 프로세스 PORT
*				db_host : 접속할 DB의 IP
*               db_port : 접속할 DB의 PORT
*               my_id : 자식 프로세스 ID
* @return       없음
*/

static pid_t create_child(int child_id, int filter_child_port, char *db_host, char *db_port)
{
	pid_t pid;
	int i;

	pid = fork();
	if ((pid_t) - 1 == pid) {
		loaderLog(L_ERR, "could not fork background process \n");
		exit(0);
	} else if (0 == pid) {
		log_init();
		signal(SIGALRM, signal_handler);
		log_write("filter %d start.", filter_child_port);
		for (i = 0; i < MAX_FILTER; i ++) {
			if (i != child_id)
				close(sock[i]);
			else
				my_sock = sock[i];
		}
		if ( listen( my_sock, 5) == -1) {
			loaderLog(L_ERR, "socket listen failed");
			exit(0);
		}
		doTheJob(db_host, db_port, child_id);
	}
	
		return (pid);
}

int main(int argc, char *argv[])
{
	int opt;
	int is_daemon = 0;
	char db_port[6];
	char db_host[96];

	setlocale(LC_ALL, "ko_KR.UTF-8");

	strcpy(db_port, "5432");
	strcpy(db_host, "127.0.0.1");

	debug_lv = 1;
	while (-1 != (opt = getopt(argc, argv, "?si:Dt:l:p:h:c:f:s"))) {
		switch (opt) {
        case 'D':
            is_daemon = 1;
            break;

        case 's':
            //save_http_stream = 1;
            break;

        case 'i':
            //pv_id = atoi(optarg);
            break;

        case 'p':
            if (strlen(optarg) <= 5)
                strcpy(db_port, optarg);

            break;

        case 'h':
			/*
            if (96 <= my_strlen(optarg)) {
                loaderLog(LOG_ERROR, "too large host argument \n");

                exit(0);
            }
            strcpy(ui_host, optarg);
			*/
            break;
        case 'l':
            debug_lv = atoi(optarg);
			break;
        case 't':
            //time_out = atoi(optarg);
            //loaderLog(LOG_FD, "loader_timeout = [%d]",time_out);
            break;
        case 'f':
			/*
            {
                int filtering = atoi(optarg);

                if (UTF8_FILTERING == filtering ||
                    EUC_KR_FILTERING == filtering ||
                    BOTH_FILTERING == filtering) {
                    keyword_filtering_mode = filtering;
                }
            }
			*/
            break;
        case 'c':
            //inform_mother = atoi(optarg);
            break;
        case '?':
        default:
            printf
                ("usage : %s -? -i <privacycenter num> -p <port number> -h <UI IP addr.> -f <filtering mode> -D \n",
                 argv[0]);
            return (0);
        }
	}

	if (0 != daemon(0, 0)) {
		//loaderLog(LOG_ERROR, "Loader: daemon() not successed, errono=[%d]",
		//		 errno);
	}

	// settings init

#if 0  // db access and ID get
	while (!dbinit(db_host, db_port)) {
		sleep(10);
	}
#endif

	struct sockaddr_in si_me;
	int filter_ports[MAX_FILTER];
	int ps_id[MAX_FILTER];
	int status;
	pid_t pid;
	int i;

	memset((char *)&si_me, 0, sizeof(si_me));
	si_me.sin_family = AF_INET;
	si_me.sin_addr.s_addr = htonl(INADDR_ANY);

	for (i = 0; i < MAX_FILTER; i++) {
		filter_ports[i] = FILTER0_PORT + i;
		sock[i] = socket(AF_INET, SOCK_STREAM, 0);
		if (sock[i] == -1) {
			loaderLog(L_ERR, "socket error");
			exit(1);
		}
#if 1 /* sclee test */
        filter_ports[0] = 8081;
#endif

		si_me.sin_port = htons(filter_ports[i]);
		if (-1 == bind(sock[i], (struct sockaddr *)&si_me, sizeof(si_me))) {
			loaderLog(L_ERR, "bind error : %d", i);
			exit(1);
		}
	}

	for (i = 0; i < MAX_FILTER; i++) {
		ps_id[i] = create_child(i, filter_ports[i], db_host, db_port);
	}

#if 1
	while (1) {
		pid = wait(&status);
		loaderLog(L_INFO, "[%d] is Died, So Restarting one!", pid);
		for (i = 0; i < MAX_FILTER; i++) {
			if (ps_id[i] == pid) {
				break;
			}
		}

		if (MAX_FILTER <= i) {
			loaderLog(L_ERR, "invalid pid value");

			exit(0);
		}

		ps_id[i] = create_child(i, filter_ports[i], db_host, db_port);
		usleep(1000);
	}
#endif

	return 0;
}
