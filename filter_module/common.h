#ifndef _COMMON_H

#define _COMMON_H           1

#include <sys/types.h>

#define SMALL_BUF_LEN       256
#define BUF_LEN             1024 * 1024 // 1M
#define LARGE_BUF_LEN       10485760    // 10M

#define SQL_BUF_LEN         BUF_LEN

#define  REQ__PASS                            0
#define  REQ__BLOCK                           0x00000001
#define  REQ__BLOCK_KEYWORD                   0x00000002
#define  REQ__BLOCK_DST_IP                    0x00000004
#define  REQ__BLOCK_TEXT_SIZE                 0x00000008
#define  REQ__BLOCK_FILE                      0x00000010
#define  REQ__BLOCK_FILE_CNT                  0x00000020
#define  REQ__BLOCK_FILE_EXT                  0x00000040
#define  REQ__BLOCK_FILE_SIZE                 0x00000080
#define  REQ__BLOCK_FILE_TYPE                 0x00000100
#define  REQ__BLOCK_FILE_KEYWORD              0x00000200
#define  REQ__BLOCK_FILE_NAME_KEYWORD         0x00000400
#define  REQ__BLOCK_JUMIN_NUM                 0x00000800
#define  REQ__BLOCK_CREDIT_CARD               0x00001000
#define  REQ__BLOCK_PHONE                     0x20000000    // 추가 3.25
#define  REQ__BLOCK_MOBILE_PHONE              0x00002000
#define  REQ__BLOCK_ACCOUNT_NUM               0x00004000
#define  REQ__BLOCK_BUSINESS_REGIST_NUM       0x00008000
#define  REQ__BLOCK_VIRUS                     0x00010000
#define  REQ__BLOCK_EMAIL                     0x00020000
#define  REQ__BLOCK_SRC_IP                    0x00040000
#define  REQ__BLOCK_FILE_JUMIN_NUM            0x00080000
#define  REQ__BLOCK_FILE_CREDIT_CARD          0x00100000
#define  REQ__BLOCK_FILE_PHONE                0x40000000    // 추가 3.25
#define  REQ__BLOCK_FILE_MOBILE_PHONE         0x00200000
#define  REQ__BLOCK_FILE_ACCOUNT_NUM          0x00400000
#define  REQ__BLOCK_FILE_BUSINESS_REGIST_NUM  0x00800000
#define  REQ__BLOCK_FILE_EMAIL                0x01000000
#define  REQ__BLOCK_DUPLICATE                 0x02000000
#define  REQ__BLOCK_TOO_LARGE                 0x04000000

#define  REQ__BLOCK_BUSINESSMAN_NUM           0x08000000
#define  REQ__BLOCK_FILE_BUSINESSMAN_NUM      0x10000000
#define  REQ__BLOCK_PASSPORT                  0x100000000   // 추가 3.26
#define  REQ__BLOCK_FILE_PASSPORT             0x200000000   // 추가 3.26

#define  REQ__BLOCK_LICENSE                   0x400000000   // 추가 3.26
#define  REQ__BLOCK_FILE_LICENSE              0x800000000   // 추가 3.26

#define  REQ__BLOCK_FOREIGN_NUM               0x1000000000    // KKH 110729
#define  REQ__BLOCK_FILE_FOREIGN_NUM          0x2000000000    // KKH 110729

#define  REQ__BLOCK_INSURENCE_NUM         0x4000000000
#define  REQ__BLOCK_FILE_INSURENCE_NUM        0x8000000000


extern void daemonize(void);
extern inline u_char my_tolower(u_char c);
extern inline size_t tolower_text(char *p);
extern inline void utoa(unsigned int n, char *buffer, unsigned int buf_len);
extern inline void replaceQuotWithSpace(char *string);
extern inline int escapeSingleQuot(char *to, char *from);
extern inline void trim_url(char *url);
extern inline int is_digit(char c);

extern inline int is_not_digit(char c);
extern inline int is_wide_char_string(char *field);
extern inline int my_strcmp(const char *s1, const char *s2);
extern inline int my_strcasecmp(const char *s1, const char *s2);
extern inline int my_strncmp(const char *s1, const char *s2, size_t n);
extern inline size_t my_strlen(const char *s);
extern char *_intoa(unsigned int addr, char *buf, unsigned int bufLen);
extern inline unsigned int trans_ip(unsigned int ip);
extern void my_system(char *cmd);
extern void get_time( char *buf );


#endif
