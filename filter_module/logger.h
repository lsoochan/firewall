#include <stdarg.h>
#include <unistd.h>

#include <wchar.h>
#include "common.h"

#define MAX_LOG_SIZE 1024 * 64  // 64K

#define LOG_PATH "/temp/filter_module/firewall/filter.log"

#define L_ERR 0
#define L_WARN 1
#define L_INFO 2
#define L_DEBUG 3
#define loaderLog( LOG , FMT, args... )  do{ \
    char time_buf[1024] = { '\0', }; \
    FILE *fp = fopen(LOG_PATH,"a"); \
    get_time( time_buf ); \
    if(debug_lv >2 )\
    debug_lv = 3; \
    switch(debug_lv) { \
        case 3: \
            if (LOG == L_DEBUG) { \
            fprintf( fp, "[%d] %s privacy %s [DEBUG] : ", getpid(), time_buf, p_name ); \
            fprintf( fp, FMT, ## args ); \
            fprintf( fp, "\n" ); \
            }\
		case 2: \
            if (LOG == L_INFO) { \
            fprintf( fp, "[%d] %s privacy %s [DEBUG] : ", getpid(), time_buf, p_name ); \
            fprintf( fp, FMT, ## args ); \
            fprintf( fp, "\n" ); \
            }\
		case 1: \
            if (LOG == L_WARN) { \
            fprintf( fp, "[%d] %s privacy %s [WARNING] : ", getpid(), time_buf, p_name ); \
            fprintf( fp, FMT, ## args ); \
            fprintf( fp, "\n" ); \
            }\
        case 0: \
            if (LOG == L_ERR) {\
            fprintf( fp, "[%d] %s privacy %s [ERROR] : ", getpid(), time_buf, p_name ); \
            fprintf( fp, FMT, ## args ); \
            fprintf( fp, "\n" ); \
            }\
            break;\
        } \
        fclose(fp); \
} while(0)

char log_buf[MAX_LOG_SIZE];
int debug_lv;
void log_init();
void log_clear();

void log_newline();
int log_append(const char *, ...);
int log_cat(const char *, ...);
int _log_cat(const char *, va_list);
void log_write(const char *, ...);
void log_flush();

extern void set_timeout_log( const char *add_log, const char *filename, const int file_size );
extern void add_timeout_log( const char *add_log );
extern void writeTimeOutLog( void );

