#ifndef MSF
#define MSF

#include "PerceptiveDocumentFilters.h"

#define  MAX_OPTION					   512

int ISYS_extract_text_on_mm( void * src, int file_size, u_char * filtering_buffer, wchar_t * filtering_wbuffer, size_t * str_len__filtering_buffer, size_t * str_len__filtering_wbuffer, int filtering_buffer_size, int filtering_wbuffer_size);
void init_msfilter(void);
LONG ISYS_extract_subfile(LONG DocHandle, LONG m_sub_size_plus, u_char * filtering_buffer, wchar_t * filtering_wbuffer, int filtering_buffer_size, int filtering_wbuffer_size);

#endif
