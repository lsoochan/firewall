/*
Copyright (c) 1988-2013, Perceptive Sofware. All rights reserved.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/****************************************************************************
* Perceptive Document Filters 11 C++ objects & helper functions
****************************************************************************/

#ifndef PERCEPTIVE_DOCFILTERSOBJECTS_H_DEF
#define PERCEPTIVE_DOCFILTERSOBJECTS_H_DEF

#include "PerceptiveDocumentFilters.h"
#include "3rdParty/Timer.h"

#include <map>
#include <string>
#include <iomanip>
#include <iostream>
#include <exception>
#include <stdio.h>

#define _UCS2(S) (WCHAR *)Perceptive::ToWCHARStr(S).c_str()

namespace Perceptive
{
class Exception: public std::exception
{
public:
  Exception(const std::string &Message) : FMessage(Message) {}
  virtual ~Exception() throw() {}
  virtual const char * what() const throw()
  {
    return(FMessage.c_str());
  }
private:
  std::string FMessage;
};

class Stream
{
public:
  Stream();
  Stream(IGR_Stream *Stream);
  virtual ~Stream();
  void Close();
  IGR_Stream *getIGRStream() const
  {
    return(FStream);
  };
protected:
  IGR_Stream *FStream;
};

class FileStream: public Stream
{
public:
  FileStream(const std::string &Filename);
};

class MemStream: public Stream
{
public:
  MemStream(void *Data, LONG DataSize);
};

typedef std::map<std::string,std::string> Metadata;
typedef std::map<std::wstring,std::wstring> MetadataW;

class DocumentFilters;
class SubFile;
class Page;
class Word;

class Extractor
{
  friend class DocumentFilters;
  friend class SubFile;
  Extractor(Stream *S);
public:
  virtual ~Extractor();
  void Open(const int Flags = IGR_BODY_AND_META, const std::string &Options = "");
  int getFileType();
  std::string getFileType(int What);
  bool getSupportsText();
  bool getSupportsSubFiles();
  bool getEOF();
  std::string GetText(const int MaxLength = 4096);
  std::wstring GetTextW(const int MaxLength = 4096);
  std::string PrepareText(const std::string &S);
  std::wstring PrepareTextW(const std::wstring &S);
  Metadata ParseMetadata(const std::string &S);
  MetadataW ParseMetadataW(const std::wstring &S);
  SubFile *GetFirstSubFile();
  SubFile *GetNextSubFile();
  SubFile *GetSubFile(const std::string &ID);
  SubFile *GetFirstImage();
  SubFile *GetNextImage();
  void CopyTo(const std::string &Filename);
  void SaveTo(const std::string &Filename);
  std::string getHashMD5();
  std::string getHashSHA1();
  void Close();
  Page *GetFirstPage();
  Page *GetNextPage();
  Page *GetPage(const int PageIndex);
  int getPageCount();
  void DumpStatistics(std::ostream &Stream);
protected:
  Stream *FStream;
  LONG FCapabilities;
  LONG FDocType;
  LONG FDocHandle;
  bool FTextEOF;
  LONG FNumChars;
  LONG FPageIndex;
  Timer FTimer;
};

class SubFile: public Extractor
{
  friend class Extractor;
  friend class Page;
  SubFile(Stream *S, const std::string &ID, const std::string &Name, LONGLONG FileTime, LONGLONG FileSize);
public:
  std::string getID();
  std::string getName();
  LONGLONG getFileDate();
  LONGLONG getFileSize();
private:
  std::string FID;
  std::string FName;
  LONGLONG FTime;
  LONGLONG FSize;
};

class Page
{
  friend class Extractor;
  friend class Canvas;
  Page(HPAGE P);
public:
  virtual ~Page();
  void Close();
  int getWordCount();
  int getWidth();
  int getHeight();
  std::string GetText(const int MaxLength = 4096);
  std::wstring GetTextW(const int MaxLength = 4096);
  Word *GetFirstWord();
  Word *GetNextWord();
  Word *GetWord(const int WordIndex);
  SubFile *GetFirstImage();
  SubFile *GetNextImage();
  void Redact(int FirstWord, int LastWord);
  void Redact(Word *FirstWord, Word *LastWord);
private:
  HPAGE FPageHandle;
  BOOL FTextEOF;
  LONG FNumChars;
  IGR_Page_Word *FWords;
  LONG FWordCount;
  LONG FWordIndex;
  void getDimensions(LONG &Width, LONG &Height);
};

class Word
{
  friend class Page;
  Word(IGR_Page_Word *W, int WordIndex);
public:
  virtual ~Word();
  std::string GetText();
  std::wstring GetTextW();
  int getX();
  int getY();
  int getWidth();
  int getHeight();
  int getStyle();
  int getCharacterOffset();
  int getWordIndex();
private:
  IGR_Page_Word *FPageWord;
  int FWordIndex;
};

class Canvas
{
  friend class DocumentFilters;
  Canvas(HCANVAS C);
public:
  virtual ~Canvas();
  void Close();
  void RenderPage(Page* P);
  void Arc(int x, int y, int x2, int y2, int x3, int y3, int x4, int y4);
  void AngleArc(int x, int y, int Radius, int StartAngle, int SweepAngle);
  void Chord(int x, int y, int x2, int y2, int x3, int y3, int x4, int y4);
  void Ellipse(int x, int y, int x2, int y2);
  void Rect(int x, int y, int x2, int y2);
  void LineTo(int x, int y);
  void MoveTo(int x, int y);
  void Pie(int x, int y, int x2, int y2, int x3, int y3, int x4, int y4);
  void RoundRect(int x, int y, int x2, int y2, int Radius);
  void TextOut(int x, int y, const std::string& Text);
  void TextOut(int x, int y, const std::wstring& Text);
  void TextRect(int x, int y, int x2, int y2, const std::string& Text, int Flags);
  void TextRect(int x, int y, int x2, int y2, const std::wstring& Text, int Flags);
  int TextWidth(const std::string& Text);
  int TextHeight(const std::string& Text);
  int TextWidth(const std::wstring& Text);
  int TextHeight(const std::wstring& Text);
  void SetPen (int Color, int Width, int Style);
  void SetBrush(int Color, int Style);
  void SetFont(const std::string& Name, int Size, int Style);
  void SetOpacity(BYTE Opacity);
  void DrawImage(int x, int y, void *ImageData, size_t ImageSize, const std::string &MimeType);
  void DrawScaleImage(int x, int y, int x2, int y2, void *ImageData, size_t ImageSize, const std::string &MimeType);
  void Rotation(int Degrees);
  void Reset();
private:
  HCANVAS FCanvasHandle;
};

class DocumentFilters
{
public:
  DocumentFilters();
  virtual ~DocumentFilters();
  void Initialize(const std::string &License, const std::string &Path);
  void Uninitialize();
  std::string GetMemoryStatus(); // NB. Windows-only memory diagnostics
  Extractor *GetExtractor(const std::string &Filename);
  Extractor *GetExtractor(Stream *S);
  void SetDefaultInputEncoding(const std::string &Encoding);
  void SetDefaultInputCodePage(const int CP);
  void SetTempPath(const std::string &Path);
  void DumpStatistics(std::ostream &S);
  Canvas* MakeOutputCanvas(const std::string& Filename, int Type, const std::string& Options);
private:
  SHORT FInstance;
};

std::wstring ToWideStr(const WCHAR *S, const int NumChars);
std::string ToUTF8Str(const WCHAR *S, const int NumChars);
std::string ToWCHARStr(const std::string &S);
std::string ToWCHARStr(const std::wstring &S);

std::string GetErrorStr(const int ReturnCode);
std::string GetFileTypeName(const int FileType);
std::string HTMLEncode(const std::string &S);
std::string ReplaceAll(const std::string &S, const std::string& Search, const std::string& Replace);

void PrepareFileForWideOutput(FILE *F, const bool IncludeBOM);
void PrepareFileForUTF8Output(FILE *F, const bool IncludeBOM);
std::string ReadTextFile(const std::string &Filename);

bool SameText(const std::string &S1, const std::string &S2);
std::string IncludeTrailingPathDelimiter(const std::string &Path);
std::string RemoveTrailingPathDelimiter(const std::string &Path);
std::string JoinFilename(const std::string &Path, const std::string &Filename);
std::string MakeSafeFilename(const std::string &Filename);
bool FileExists(const std::string &Filename);
bool DirExists(const std::string &Path);
bool CreateFolder(const std::string &Path);

}; // namespace Perceptive

#endif // PERCEPTIVE_DOCFILTERSOBJECTS_H_DEF
