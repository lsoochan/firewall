#include "PerceptiveDocumentFiltersLicense.inc"
#include "PerceptiveDocumentFilters.h"
#include <syslog.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <iconv.h>
#include "dfilter.h"
#include "pvunicode.h"

static wchar_t ISYS_woption[MAX_OPTION*2 + 1];
static char ISYS_option[MAX_OPTION];

static wchar_t ISYS_ID[4096];
static wchar_t ISYS_Name[1024];
enum charSet { DEFAULT, MSCP949, UTF8 };

inline u_char my_tolower(u_char c)
{
	if ('A' <= c && c <= 'Z')
		c -= ('A' - 'a');
	if (14 == c)
		c = 10;
	return (c);
}

int ISYS_extract_text_on_mm( void * src, int file_size, u_char * filtering_buffer, wchar_t * filtering_wbuffer, size_t * str_len__filtering_buffer, size_t * str_len__filtering_wbuffer
		, int filtering_buffer_size, int filtering_wbuffer_size)
{
	int i;
	clock_t s_time, e_time;
	LONG DocHandle, cp, dt;
	LONG RC1, RC2, RC3;
	LONG size = filtering_wbuffer_size;
	LONG sub_size_plus = 0;
	struct IGR_Stream *pStream = NULL;
	ECB_Type ISYSError;

	syslog(LOG_ERR,"welcome msfilter");

	s_time = clock();
		syslog(LOG_ERR,"IGR_Make_Stream_From_Memory() start");
	RC1 = IGR_Make_Stream_From_Memory(src, file_size, NULL, &pStream, &ISYSError);
	if (RC1 != IGR_OK) {
		syslog(LOG_ERR,"IGR_Make_Stream_From_Memory() : failed code[%d]",RC1);
		return -1;
	}

	syslog(LOG_ERR,"IGR_Open_Stream() start");
	RC2 = IGR_Open_Stream_Ex(pStream, IGR_BODY_ONLY, (LPCWSTR)ISYS_woption, &cp, &dt, &DocHandle, &ISYSError);
	if (RC2 != IGR_OK) {
		syslog(LOG_ERR,"IGR_Open_Stream() : failed. It may be a text file : [%d]",RC2);
		convToUTF8(getCharset(src, file_size), src, file_size, (char *)filtering_buffer, filtering_buffer_size);
		*str_len__filtering_buffer = strlen((char *)filtering_buffer);
		for( i = 0; i < file_size; i++) {
			filtering_buffer[i] = my_tolower( filtering_buffer[i] );
		}
		*str_len__filtering_wbuffer = mbstowcs( filtering_wbuffer, (char *)filtering_buffer, filtering_wbuffer_size );
		pStream->Close(pStream);
		return 1;
	}
	memset(filtering_buffer, '\0', filtering_buffer_size*sizeof(char));

	if((cp & 2) || (dt == 56)) {		// need to check subfile
		sub_size_plus = ISYS_extract_subfile(DocHandle, 0, filtering_buffer, filtering_wbuffer, filtering_buffer_size, filtering_wbuffer_size);
		RC3 = IGR_Get_Text(DocHandle, (LPWSTR)filtering_wbuffer, &size, &ISYSError);

		if(RC3 == IGR_OK) {
			*str_len__filtering_wbuffer = size;
			syslog(LOG_ERR,"widechar_to_UTF8 start");
			Widechar_to_UTF8((LPCWSTR)filtering_wbuffer, (LPSTR)(filtering_buffer+sub_size_plus), filtering_buffer_size-sub_size_plus);
			sub_size_plus = strlen((char *)filtering_buffer);
		}

	}else {			// it is only text
			RC3 = IGR_Get_Text(DocHandle, (LPWSTR)filtering_wbuffer, &size, &ISYSError);
			if(RC3 == IGR_OK) {
				*str_len__filtering_wbuffer = size;
				Widechar_to_UTF8((LPCWSTR)filtering_wbuffer, (LPSTR)filtering_buffer, filtering_buffer_size);
			}

	}
	e_time = clock();
	syslog(LOG_ERR,"isys time spent : %.2f",((double)(e_time - s_time))/CLOCKS_PER_SEC);
	*str_len__filtering_buffer = strlen((char *)filtering_buffer);
	for( i = 0; i < file_size; i++ ) {
		filtering_buffer[i] = my_tolower( filtering_buffer[i] );
	}
	*str_len__filtering_wbuffer = mbstowcs( filtering_wbuffer, (char *)filtering_buffer, filtering_wbuffer_size );
	
	syslog(LOG_ERR,"end msfilter");
	IGR_Close_File(DocHandle, &ISYSError);
	pStream->Close(pStream);

	return 1;
}

LONG ISYS_extract_subfile(LONG DocHandle, LONG m_sub_size_plus, u_char * filtering_buffer, wchar_t * filtering_wbuffer, int filtering_buffer_size, int filtering_wbuffer_size)
{
	struct IGR_Stream *subStream = NULL;
	LONG RC3;
	LONG sub_size = filtering_wbuffer_size;
	LONG sub_size_plus = m_sub_size_plus;
	ECB_Type ISYSError;
	int64_t FileDate, FileSize;

	syslog(LOG_ERR,"extract sub");
	while(1) {
		LONG subDocHandle,sub_cp, sub_dt;
		RC3 = IGR_Get_Subfile_Entry(DocHandle, (LPWSTR)ISYS_ID, (LPWSTR)ISYS_Name, (PLONGLONG)&FileDate, (PLONGLONG)&FileSize, &ISYSError);
		if(RC3 != IGR_OK) {
			break;
		}
		RC3 = IGR_Extract_Subfile_Stream(DocHandle, (LPCWSTR)ISYS_ID, &subStream, &ISYSError);
		if(RC3 == IGR_OK) {
			RC3 = IGR_Open_Stream_Ex(subStream, IGR_BODY_ONLY, (LPCWSTR)ISYS_woption, &sub_cp, &sub_dt, &subDocHandle, &ISYSError);
			if(RC3 != IGR_OK) {
				syslog(LOG_ERR,"IGR_Open_Stream_Ex() subfile : failed code[%d]", RC3);
				subStream->Close(subStream);
				continue;
			}

			// 서브 파일 체크(재귀함수)
			if((sub_cp & 2) || (sub_dt == 56))
				sub_size_plus = ISYS_extract_subfile(subDocHandle, sub_size_plus, filtering_buffer, filtering_wbuffer, filtering_buffer_size, filtering_wbuffer_size);

			RC3 = IGR_Get_Text(subDocHandle, (LPWSTR)filtering_wbuffer, &sub_size, &ISYSError);
			sub_size = filtering_wbuffer_size;
			if(RC3 != IGR_OK) {
				syslog(LOG_ERR, "IGR_Get_Text() subfile : failed code[%d], message : %s",RC3, ISYSError.Msg);
				IGR_Close_File(subDocHandle, &ISYSError);
				subStream->Close(subStream);
				continue;
			}
			Widechar_to_UTF8((LPCWSTR)filtering_wbuffer, (LPSTR)(filtering_buffer+sub_size_plus), filtering_buffer_size-sub_size_plus);
			sub_size_plus = strlen((char *)filtering_buffer);
			IGR_Close_File(subDocHandle, &ISYSError);
			subStream->Close(subStream);
		}
		syslog(LOG_ERR,"filter%s",filtering_buffer);
	}

	return sub_size_plus;	
}

void init_msfilter(void)
{
	ECB_Type ISYSError;
	SHORT ISYSHandle;
	Instance_Status_Block ISB;
	size_t L1 = 949;
	size_t L2 = 0;
	strcpy(ISYS_option, "EXCELMODE=CSV;STYLESHEET=Off;IMAGES=No;SHOWHIDDEN=Visible;HDISOLATIONMAXTIME=2;PDFXMPMETA=Off;PDFBOOKMARKS=off;PDFANNOTATIONS=Off;PDFIMAGES=No;PDFMAXPROCESSINGTIME=5;PDFTHUMBNAILS=No;");
	strncpy(ISB.Licensee_ID1, LICENSE_KEY, sizeof(ISB.Licensee_ID1));
	Init_Instance(0, ".", &ISB, &ISYSHandle, &ISYSError);
	UTF8_to_Widechar(ISYS_option, (LPWSTR)ISYS_woption, MAX_OPTION);
	syslog(LOG_ERR, "start msfilter");
	if(strlen(ISYSError.Msg) > 0) {
		syslog(LOG_ERR, "Init_Instance : Unable to initialize ISYS Document Filters : %s",ISYSError.Msg);
		exit(1);
	}
	IGR_Multiplex(IGR_Multi_Set_Code_Page, &L1, &L2, &ISYSError);
	if(strlen(ISYSError.Msg) > 0) {
		syslog(LOG_ERR,"IGR_Multiplex : failed : %s", ISYSError.Msg);
	}
}
