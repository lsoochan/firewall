
extern void init_pvunicode(void);
extern size_t convToUTF8(int, char *from, size_t from_size, char *utf8,
			 size_t utf8_size);
extern size_t convMS949toUTF8(char *ms949, size_t ms949_size, char *utf8,
			      size_t utf8_size);
extern size_t convEUCKRtoUTF8(char *euckr, size_t euckr_size, char *utf8,
			      size_t utf8_size);
extern const char *convCERtoNCR(const char *cer);
extern unsigned int convNCRtoCP(const char *ncr);
extern size_t convCPtoUTF8(unsigned int cp, char *utf8, size_t utf8_size);
extern size_t convCERtoUTF8(char *cer, char *utf8, size_t utf8_size);
extern size_t convCERnNCRtoUTF8(const char *field, char *buf, size_t buf_size);
extern int getCharset(char *str, size_t str_size);
