#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iconv.h>
#include "pvunicode.h"

#define BUF_LEN 512 
#define nrCharEntity 252


static const char *charEntityTable[nrCharEntity][2] = {
//              {"Character Entity Reference(CER)", "Numeric character reference(NCR)"}
	{"&amp;", "&#38;"}, {"&gt;", "&#62;"}, {"&lt;", "&#60;"}, {"&quot;",
								   "&#34;"},
	    {"&acute;", "&#180;"},
	{"&cedil;", "&#184;"}, {"&circ;", "&#710;"}, {"&macr;", "&#175;"},
	    {"&middot;", "&#183;"}, {"&tilde;", "&#732;"},
	{"&uml;", "&#168;"}, {"&Aacute;", "&#193;"}, {"&aacute;", "&#225;"},
	    {"&Acirc;", "&#194;"}, {"&acirc;", "&#226;"},
	{"&AElig;", "&#198;"}, {"&aelig;", "&#230;"}, {"&Agrave;", "&#192;"},
	    {"&agrave;", "&#224;"}, {"&Aring;", "&#197;"},
	{"&aring;", "&#229;"}, {"&Atilde;", "&#195;"}, {"&atilde;", "&#227;"},
	    {"&Auml;", "&#196;"}, {"&auml;", "&#228;"},
	{"&Ccedil;", "&#199;"}, {"&ccedil;", "&#231;"}, {"&Eacute;", "&#201;"},
	    {"&eacute;", "&#233;"}, {"&Ecirc;", "&#202;"},
	{"&ecirc;", "&#234;"}, {"&Egrave;", "&#200;"}, {"&egrave;", "&#232;"},
	    {"&ETH;", "&#208;"}, {"&eth;", "&#240;"},
	{"&Euml;", "&#203;"}, {"&euml;", "&#235;"}, {"&Iacute;", "&#205;"},
	    {"&iacute;", "&#237;"}, {"&Icirc;", "&#206;"},
	{"&icirc;", "&#238;"}, {"&Igrave;", "&#204;"}, {"&igrave;", "&#236;"},
	    {"&Iuml;", "&#207;"}, {"&iuml;", "&#239;"},
	{"&Ntilde;", "&#209;"}, {"&ntilde;", "&#241;"}, {"&Oacute;", "&#211;"},
	    {"&oacute;", "&#243;"}, {"&Ocirc;", "&#212;"},
	{"&ocirc;", "&#244;"}, {"&OElig;", "&#338;"}, {"&oelig;", "&#339;"},
	    {"&Ograve;", "&#210;"}, {"&ograve;", "&#242;"},
	{"&Oslash;", "&#216;"}, {"&oslash;", "&#248;"}, {"&Otilde;", "&#213;"},
	    {"&otilde;", "&#245;"}, {"&Ouml;", "&#214;"},
	{"&ouml;", "&#246;"}, {"&Scaron;", "&#352;"}, {"&scaron;", "&#353;"},
	    {"&szlig;", "&#223;"}, {"&THORN;", "&#222;"},
	{"&thorn;", "&#254;"}, {"&Uacute;", "&#218;"}, {"&uacute;", "&#250;"},
	    {"&Ucirc;", "&#219;"}, {"&ucirc;", "&#251;"},
	{"&Ugrave;", "&#217;"}, {"&ugrave;", "&#249;"}, {"&Uuml;", "&#220;"},
	    {"&uuml;", "&#252;"}, {"&Yacute;", "&#221;"},
	{"&yacute;", "&#253;"}, {"&yuml;", "&#255;"}, {"&Yuml;", "&#376;"},
	    {"&cent;", "&#162;"}, {"&curren;", "&#164;"},
	{"&euro;", "&#8364;"}, {"&pound;", "&#163;"}, {"&yen;", "&#165;"},
	    {"&brvbar;", "&#166;"}, {"&bull;", "&#8226;"},
	{"&copy;", "&#169;"}, {"&dagger;", "&#8224;"}, {"&Dagger;", "&#8225;"},
	    {"&frasl;", "&#8260;"}, {"&hellip;", "&#8230;"},
	{"&iexcl;", "&#161;"}, {"&image;", "&#8465;"}, {"&iquest;", "&#191;"},
	    {"&lrm;", "&#8206;"}, {"&mdash;", "&#8212;"},
	{"&ndash;", "&#8211;"}, {"&not;", "&#172;"}, {"&oline;", "&#8254;"},
	    {"&ordf;", "&#170;"}, {"&ordm;", "&#186;"},
	{"&para;", "&#182;"}, {"&permil;", "&#8240;"}, {"&prime;", "&#8242;"},
	    {"&Prime;", "&#8243;"}, {"&real;", "&#8476;"},
	{"&reg;", "&#174;"}, {"&rlm;", "&#8207;"}, {"&sect;", "&#167;"},
	    {"&shy;", "&#173;"}, {"&sup1;", "&#185;"},
	{"&trade;", "&#8482;"}, {"&weierp;", "&#8472;"}, {"&bdquo;", "&#8222;"},
	    {"&laquo;", "&#171;"}, {"&ldquo;", "&#8220;"},
	{"&lsaquo;", "&#8249;"}, {"&lsquo;", "&#8216;"}, {"&raquo;", "&#187;"},
	    {"&rdquo;", "&#8221;"}, {"&rsaquo;", "&#8250;"},
	{"&rsquo;", "&#8217;"}, {"&sbquo;", "&#8218;"}, {"&emsp;", "&#8195;"},
	    {"&ensp;", "&#8194;"}, {"&nbsp;", "&#160;"},
	{"&thinsp;", "&#8201;"}, {"&zwj;", "&#8205;"}, {"&zwnj;", "&#8204;"},
	    {"&deg;", "&#176;"}, {"&divide;", "&#247;"},
	{"&frac12;", "&#189;"}, {"&frac14;", "&#188;"}, {"&frac34;", "&#190;"},
	    {"&ge;", "&#8805;"}, {"&le;", "&#8804;"},
	{"&minus;", "&#8722;"}, {"&sup2;", "&#178;"}, {"&sup3;", "&#179;"},
	    {"&times;", "&#215;"}, {"&alefsym;", "&#8501;"},
	{"&and;", "&#8743;"}, {"&ang;", "&#8736;"}, {"&asymp;", "&#8776;"},
	    {"&cap;", "&#8745;"}, {"&cong;", "&#8773;"},
	{"&cup;", "&#8746;"}, {"&empty;", "&#8709;"}, {"&equiv;", "&#8801;"},
	    {"&exist;", "&#8707;"}, {"&fnof;", "&#402;"},
	{"&forall;", "&#8704;"}, {"&infin;", "&#8734;"}, {"&int;", "&#8747;"},
	    {"&isin;", "&#8712;"}, {"&lang;", "&#9001;"},
	{"&lceil;", "&#8968;"}, {"&lfloor;", "&#8970;"}, {"&lowast;",
							  "&#8727;"},
	    {"&micro;", "&#181;"}, {"&nabla;", "&#8711;"},
	{"&ne;", "&#8800;"}, {"&ni;", "&#8715;"}, {"&notin;", "&#8713;"},
	    {"&nsub;", "&#8836;"}, {"&oplus;", "&#8853;"},
	{"&or;", "&#8744;"}, {"&otimes;", "&#8855;"}, {"&part;", "&#8706;"},
	    {"&perp;", "&#8869;"}, {"&plusmn;", "&#177;"},
	{"&prod;", "&#8719;"}, {"&prop;", "&#8733;"}, {"&radic;", "&#8730;"},
	    {"&rang;", "&#9002;"}, {"&rceil;", "&#8969;"},
	{"&rfloor;", "&#8971;"}, {"&sdot;", "&#8901;"}, {"&sim;", "&#8764;"},
	    {"&sub;", "&#8834;"}, {"&sube;", "&#8838;"},
	{"&sum;", "&#8721;"}, {"&sup;", "&#8835;"}, {"&supe;", "&#8839;"},
	    {"&there4;", "&#8756;"}, {"&Alpha;", "&#913;"},
	{"&alpha;", "&#945;"}, {"&Beta;", "&#914;"}, {"&beta;", "&#946;"},
	    {"&Chi;", "&#935;"}, {"&chi;", "&#967;"},
	{"&Delta;", "&#916;"}, {"&delta;", "&#948;"}, {"&Epsilon;", "&#917;"},
	    {"&epsilon;", "&#949;"}, {"&Eta;", "&#919;"},
	{"&eta;", "&#951;"}, {"&Gamma;", "&#915;"}, {"&gamma;", "&#947;"},
	    {"&Iota;", "&#921;"}, {"&iota;", "&#953;"},
	{"&Kappa;", "&#922;"}, {"&kappa;", "&#954;"}, {"&Lambda;", "&#923;"},
	    {"&lambda;", "&#955;"}, {"&Mu;", "&#924;"},
	{"&mu;", "&#956;"}, {"&Nu;", "&#925;"}, {"&nu;", "&#957;"}, {"&Omega;",
								     "&#937;"},
	    {"&omega;", "&#969;"},
	{"&Omicron;", "&#927;"}, {"&omicron;", "&#959;"}, {"&Phi;", "&#934;"},
	    {"&phi;", "&#966;"}, {"&Pi;", "&#928;"},
	{"&pi;", "&#960;"}, {"&piv;", "&#982;"}, {"&Psi;", "&#936;"}, {"&psi;",
								       "&#968;"},
	    {"&Rho;", "&#929;"},
	{"&rho;", "&#961;"}, {"&Sigma;", "&#931;"}, {"&sigma;", "&#963;"},
	    {"&sigmaf;", "&#962;"}, {"&Tau;", "&#932;"},
	{"&tau;", "&#964;"}, {"&Theta;", "&#920;"}, {"&theta;", "&#952;"},
	    {"&thetasym;", "&#977;"}, {"&upsih;", "&#978;"},
	{"&Upsilon;", "&#933;"}, {"&upsilon;", "&#965;"}, {"&Xi;", "&#926;"},
	    {"&xi;", "&#958;"}, {"&Zeta;", "&#918;"},
	{"&zeta;", "&#950;"}, {"&crarr;", "&#8629;"}, {"&darr;", "&#8595;"},
	    {"&dArr;", "&#8659;"}, {"&harr;", "&#8596;"},
	{"&hArr;", "&#8660;"}, {"&larr;", "&#8592;"}, {"&lArr;", "&#8656;"},
	    {"&rarr;", "&#8594;"}, {"&rArr;", "&#8658;"},
	{"&uarr;", "&#8593;"}, {"&uArr;", "&#8657;"}, {"&clubs;", "&#9827;"},
	    {"&diams;", "&#9830;"}, {"&hearts;", "&#9829;"},
	{"&spades;", "&#9824;"}, {"&loz;", "&#9674;"}

};

enum charSet { DEFAULT, MSCP949, UTF8 };
static iconv_t cd_from_ascii_to_utf8;
static iconv_t cd_from_mscp949_to_utf8;
static iconv_t cd_from_utf8_to_utf8;

static char temp_buf_limited[BUF_LEN];
static char *temp_buf;

void init_pvunicode(void)
{
	cd_from_ascii_to_utf8 = iconv_open("UTF-8//IGNORE//TRANSLIT", "ASCII");
	cd_from_mscp949_to_utf8 =
	    iconv_open("UTF-8//IGNORE//TRANSLIT", "MSCP949");
	cd_from_utf8_to_utf8 = iconv_open("UTF-8//IGNORE//TRANSLIT", "UTF-8");
	if ((iconv_t) - 1 == cd_from_ascii_to_utf8 ||
	    (iconv_t) - 1 == cd_from_mscp949_to_utf8 ||
	    (iconv_t) - 1 == cd_from_utf8_to_utf8) {
		//loaderLog(LOG_ERR, "iconv_open() fails");
		exit(0);
	}
}

/*
 * fromCode에서 UTF-8 인코딩으로 변경함.
 */
size_t
convToUTF8(int fromCode, char *from, size_t from_size, char *utf8,
	   size_t utf8_size)
{
	char *temp_buf_head = NULL;
	size_t temp_buf_size = 0;
	size_t ret = 0;

	// Safe guard when caller doesn't care about last one byte( reserved for null )
	utf8_size--;

	if (NULL == from || from_size <= 0 || NULL == utf8 || utf8_size <= 0)
		return ((size_t) - 1);

	if (DEFAULT != fromCode && MSCP949 != fromCode && UTF8 != fromCode)
		return ((size_t) - 1);

	memset(temp_buf_limited, '\0', BUF_LEN);

	if (utf8_size <= BUF_LEN)
		temp_buf = temp_buf_limited;
	else
		temp_buf = (char *)calloc(utf8_size, sizeof(char));

	temp_buf_head = temp_buf;
	temp_buf_size = utf8_size;

	if (MSCP949 == fromCode) {
		ret = iconv(cd_from_mscp949_to_utf8, (char **)&from, &from_size,
			    (char **)&temp_buf, &temp_buf_size);
	} else if (DEFAULT == fromCode) {
		ret = iconv(cd_from_ascii_to_utf8, (char **)&from, &from_size,
			    (char **)&temp_buf, &temp_buf_size);
	} else if (UTF8 == fromCode) {
		ret = iconv(cd_from_utf8_to_utf8, (char **)&from, &from_size,
			    (char **)&temp_buf, &temp_buf_size);
	}

	*temp_buf = '\0';

	convCERnNCRtoUTF8(temp_buf_head, utf8, utf8_size);

	if (temp_buf_head != temp_buf_limited)
		free(temp_buf_head);

	return (ret);
}

size_t
convMS949toUTF8(char *ms949, size_t ms949_size, char *utf8, size_t utf8_size)
{
	return convToUTF8(MSCP949, ms949, ms949_size, utf8, utf8_size);
}

/*
size_t 
convEUCKRtoUTF8(char* euckr, size_t euckr_size, char* utf8, size_t utf8_size)
{
    return convMS949toUTF8(euckr, euckr_size, utf8, utf8_size);
}
*/

/*
 * CER(Character Entity Reference)(위에 선언된 2차원 배열)를 UTF-8로 변경할 수 있도록 NCR(Numeric Character Reference)로 변경한다.
 */
const char *convCERtoNCR(const char *cer)
{
	int i = 0;
	int j = 0;

	if (cer == NULL)
		return NULL;

	if (cer[0] != '&')
		return NULL;

	// cer may not be terminated with NULL.
	for (i = 0; i < nrCharEntity; i++) {
		if (cer[1] == charEntityTable[i][0][1]) {
			for (j = 2;
			     cer[j] != ';' && charEntityTable[i][0][j] != ';'
			     && j < 10; j++) {
				if (cer[j] != charEntityTable[i][0][j])
					break;
			}

			if ((cer[j] == charEntityTable[i][0][j])
			    && cer[j] == ';')
				return charEntityTable[i][1];	// return NCR correspond to CER
		}
	}

	return NULL;
}

// Unicode codepoint (CP)
// NCR(Numeric character reference) 예)&#x13;과 같은 코드 포인트를 표현한 HTML 내의 NCR을 UTF-8로 변경함.
unsigned int convNCRtoCP(const char *ncr)
{
	char ncr_buf[32] = { '\0', };
	int index = 0;
	int start = 0;

	if (ncr == NULL)
		return 0;

	if (ncr[0] != '&' || ncr[1] != '#')
		return 0;

	start = 2;

	while (ncr[start + index] != 0 && ncr[start + index] != ';'
	       && index < 10) {
		ncr_buf[index] = ncr[index + start];
		index++;
	}

	// NCR must end with ';'
	if (ncr[start + index] == ';') {

		// ncr[0] == 'X' is invalid however, check loosely here
		if (ncr_buf[0] == 'x' || ncr_buf[0] == 'X')
			return (unsigned int)strtoul(ncr_buf + 1, NULL, 16);
		else
			return (unsigned int)strtoul(ncr_buf, NULL, 10);
	}

	return 0;
}

// Convert codepoint to utf-8 multibyte chars with null terminator. Support both decimal and hex format.
// Code point를 UTF-8 멀티 바이트 문자로 변경함. 
size_t convCPtoUTF8(unsigned int cp, char *utf8, size_t utf8_size)
{
	if (utf8 == NULL || utf8_size <= 0 || cp < 0)
		return -1;

	if (cp < 0x80 && utf8_size >= 1) {
		utf8[0] = (char)cp;
		utf8[1] = '\0';
		return 1;
	} else if (cp < 0x800 && utf8_size >= 2) {
		utf8[0] = (0xC0 | (cp >> 6));
		utf8[1] = (0x80 | (cp & 0x3F));
		utf8[2] = '\0';
		return 2;
	} else if (cp < 0x10000 && utf8_size >= 3) {
		utf8[0] = (0xE0 | (cp >> 12));
		utf8[1] = (0x80 | (cp >> 6 & 0x3F));
		utf8[2] = (0x80 | (cp & 0x3F));
		utf8[3] = '\0';
		return 3;
	} else if (cp < 0x200000 && utf8_size >= 4) {
		utf8[0] = (0xF0 | (cp >> 18));
		utf8[1] = (0x80 | (cp >> 12 & 0x3F));
		utf8[2] = (0x80 | (cp >> 6 & 0x3F));
		utf8[3] = (0x80 | (cp & 0x3F));
		utf8[4] = '\0';
		return 4;
	}

	return -1;
}

size_t convCERtoUTF8(char *cer, char *utf8, size_t utf8_size)
{
	if (cer == NULL || utf8 == NULL || utf8_size <= 0)
		return -1;

	return convCPtoUTF8(convNCRtoCP(convCERtoNCR(cer)), utf8, utf8_size);
}

/*
 * field의 내용 중 CER과 NCR을 UTF-8로 우선 변경한다.
 */
size_t convCERnNCRtoUTF8(const char *field, char *buf, size_t buf_size)
{
	size_t convSize = 0;
	int i = 0;
	int buf_index = 0;
	char utf8[5] = { '\0', };
	unsigned int length = 0;
	unsigned int cp = 0;

	length = strlen(field);

	//loaderLog( LOG_FD, "Convert CER & NCR to UTF8, length = %d: \n", length);
	for (i = 0; i < length && buf_index < buf_size; i++) {
		cp = 0;
		if (field[i] == '&') {
			if (field[i + 1] == '#')	// May be a Numeric character reference
			{
				cp = convNCRtoCP((const char *)(field + i));
			} else	// May be a character entity reference
			{
				cp = convNCRtoCP(convCERtoNCR(field + i));
			}

			if (cp > 0) {
				memset(utf8, '\0', sizeof(utf8));

				convSize = convCPtoUTF8(cp, utf8, 5);

				memcpy(buf + buf_index, utf8, convSize);

				buf_index += convSize;

				while (field[i] != ';')
					i++;
			}
		}

		if (cp <= 0) {
			buf[buf_index++] = field[i];
		}
	}

	buf[buf_index] = '\0';

	//loaderLog( LOG_FD, "before : \"%s\"\n", field);
	//loaderLog( LOG_FD, "after : \"%s\"\n", buf);

	return (buf_index - 1);
}

int getCharset(char *str, size_t str_size)
{
	unsigned int ch[3];
	int i;
	int count = ((int)str_size) - 2;

	//WARN : str_size - 2 (int) 캐스트가 없으면 size_t로 형 변환되어 계산되므로 문제가 발생할 수 있다.

	for (i = 0; i < count; i++) {
		ch[0] = (unsigned int)str[i] & 0xFF;
		ch[1] = (unsigned int)str[i + 1] & 0xFF;
		ch[2] = (unsigned int)str[i + 2] & 0xFF;

		if ((ch[0] >> 4) == 0xE && (ch[1] >> 6) == 0x2
		    && (ch[2] >> 6) == 0x2) {
			//loaderLog( LOG_FD, "ITS UTF8 %u", str_size);
			return (UTF8);
		}

		if ((0x81 <= ch[0] && ch[0] < 0xC8)
		    && (0x41 <= ch[1] && ch[1] <= 0xFE)) {
			//loaderLog( LOG_FD, "ITS MSCP949 %u", str_size);
			return (MSCP949);
		}
	}

	ch[0] = (unsigned int)str[i] & 0xFF;
	ch[1] = (unsigned int)str[i + 1] & 0xFF;
	if ((0x81 <= ch[0] && ch[0] < 0xC8) && (0x41 <= ch[1] && ch[1] <= 0xFE)) {
		//loaderLog( LOG_FD, "ITS MSCP949 %u", str_size);
		return (MSCP949);
	}
	// loaderLog( LOG_FD, "ITS ASCII %u", str_size);
	return (DEFAULT);
}
