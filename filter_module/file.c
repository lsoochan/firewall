#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>

#include "logger.h"
#include "filter_module.h"

#define p_name "filter_module"

char cmd_buf[4096];

const char *get_file_ext(char *filename)
{
    int length;
    int i;

    if (NULL == filename)
        return NULL;

    length = strlen(filename);

    for (i = length - 1; 0 <= i; i--) {
        if (0 != (0x80 & filename[i])) {
            return NULL;
        }

        if (filename[i] == '.')
            break;
    }

    if (i < 0 || (length - 1) == i)
        return NULL;

    if (6 < (length - i))
        return NULL;
    else
        return (filename + i);
}

size_t write_afile(const char *filepath, unsigned int len, const void *content)
{
    FILE *out = NULL;
    size_t ret = -1;
    if (NULL == filepath) {
        loaderLog(L_ERR, "write_afile() : NULL == filepath");
        return (-1);
    }
    if (0 == len) {
        loaderLog(L_ERR, "write_afile() : 0 == len");
        return (-1);
    }
    if (NULL == content) {
        loaderLog(L_ERR, "write_afile() : NULL == content");
        return (-1);
    }

    out = fopen(filepath, "w");
    ret = fwrite(content, 1, len, out);
    fclose(out);

    return ret;
}

char *read_http_file(struct _message *msg)
{
    char *stream;
    char filename[MAX_AFILENAME_SIZE];
    long read_max_size;
    int read_size;
    struct stat buf;
    int fd;
    int i;

    memset(filename, 0, MAX_AFILENAME_SIZE);
    strncpy(filename, msg->file_path, msg->file_path_size);
    fd = open(filename, O_RDONLY);
    if (fd < 0) {
        loaderLog(L_ERR, "file open failed!\n");
        return NULL;
    }

    if (fstat(fd, &buf)) {
        loaderLog(L_ERR, "stat failed!\n");
        return NULL;
    }

    stream = malloc(sizeof(char) * buf.st_size);
    if (stream == NULL) {
        loaderLog(L_ERR, "Memory alloc failed!\n");
        return NULL;
    }
    if (buf.st_size > SSIZE_MAX)
        read_max_size = SSIZE_MAX;
    else
        read_max_size = buf.st_size;

    read_size = 0;
    do {
        i = read(fd, stream + read_size, read_max_size);
        read_size += i;
    } while (read_size < buf.st_size);
    loaderLog(L_DEBUG, "Read Data = [%d]\n", read_size);
#if 0 /* sclee test */
	msg->file_size = read_size;
#endif
    return stream;
}


