#include "db.h"
#include "common.h"

#define MAX_FILTER		10
#define MAX_MSG_SIZE	1024 * 64
#define MAX_PACKET_SIZE	(32 * 1024 * 1024)
#define MAX_AFILENAME_SIZE 512
#define MAX_FIELDS_NUM                 100
#define MAX_AFILES_NUM                  20
#define MAX_KEYWORD_CNT			20000
#define  MAX_PATTERN_CNT              20000


#define LOG__HTTP_SIZE               56    /*  4 X 12 + 2 X 2 + 4 */
#define LOG__HTTP_FIELD_SIZE         12    /*  4 X 3  */
#define LOG__HTTP_FILE_SIZE          24    /*  4 X 6  */
#define LOG__BLOCK_FILE_SIZE          8    /*  4 X 2  */
#define LOG__BLOCK_FILE_SIZE_SIZE    16    /*  4 X 4  */
#define LOG__BLOCK_FILE_EXT_SIZE      8    /*  4 X 2  */
#define LOG__BLOCK_SRC_IP_SIZE        8    /*  4 X 2  */
#define LOG__AFILE_SIZE              16    /*  4 X 4  */


#define PARSING_OK		1
#define PARSING_ERROR	0

#define FILTER0_PORT	7071
#define FILTER1_PORT    7072
#define FILTER2_PORT    7073
#define FILTER3_PORT    7074
#define FILTER4_PORT    7075
#define FILTER5_PORT    7076
#define FILTER6_PORT    7077
#define FILTER7_PORT    7078
#define FILTER8_PORT    7079
#define FILTER9_PORT    7080

#define  KIND__FIELD               0x02
#define  KIND__FILE                0x04


enum EncType { ENC_TYPE_UNKNOWN = 0, MULTI_PART, URL_ENCODED, PLAIN_TEXT };
enum HTTP_Request { REQUEST_UNKNOWN =
	0, GET, POST, PUT, DELETE, HEAD, TRACE, OPTIONS, CONNECT };


struct _tlv {
	u_int32_t kind;
	char data[1];
};

struct _field {
    u_int32_t kind;
    u_int32_t name_len;
    u_int32_t value_len;
    char data[1];
};

struct _file {
    u_int32_t kind;
    u_int32_t type;
    u_int32_t size;
    u_int32_t name_len;
    u_int32_t fname_len;
    u_int32_t path_len;
    char data[1];
};

struct _message {
	u_int32_t magic_num;
	u_int32_t message_size;
	u_int32_t request_id;
	u_int32_t flag;
	u_int32_t file_path_size;
	char file_path[1];
};

struct _response {
	u_int32_t magic;
	u_int32_t len;
	u_int32_t rid;
	u_int32_t flag;
	u_int32_t rcode;
	u_int32_t mlen;
	char mess[256];
};

struct _http_tlv {
    u_int32_t tag;
    u_int32_t http_id;
    u_int32_t pass;
    u_int32_t pstore_id;
    u_int32_t method;
    u_int32_t src_ip;
    u_int32_t dst_ip;
    u_int16_t src_port;
    u_int16_t dst_port;
    u_int32_t url_len;
    u_int32_t referer_len;
    u_int32_t cnt;
    u_int32_t transfer_type;
    u_int32_t content_size;
    u_int32_t filename_len;
    char data[1];
};


/********************* file.c *********************/
extern char *read_http_file(struct _message *msg);
extern size_t write_afile(const char *filepath, unsigned int len, const void *content);
extern const char *get_file_ext(char *filename);
/**************************************************/

static inline char *field_Name(struct _field *f)
{
    if (NULL == f)
        return (NULL);

    return ((char *)&(f->data[0]));
}

static inline char *field_Value(struct _field *f)
{
    if (NULL == f)
        return (NULL);

    return ((char *)&(f->data[f->name_len]));
}

static inline int field_Value_Len(struct _field *f)
{
    if (NULL == f)
        return (0);

    return (f->value_len);
}

static inline void tolower_fieldName(struct _field *f)
{
    char *name;
    int len;
    int i;

    if (NULL == f)
        return;

    len = f->name_len;
    name = (char *)&(f->data[0]);

    for (i = 0; i < len; i++) {
        *name = my_tolower(*name);
        name += 1;
    }
}

static inline int whatKind(void *p)
{
    struct _tlv *tlv;

    tlv = (struct _tlv *)p;
    return (tlv->kind);
}

static inline int fieldSize(struct _field *field)
{
    return (LOG__HTTP_FIELD_SIZE + field->name_len + field->value_len);
}

static inline int fileSize(struct _file *file)
{
    return (LOG__HTTP_FILE_SIZE + file->name_len + file->fname_len +
        file->path_len + file->size);
}

