#include <stdio.h>
#include <syslog.h>
#include <stdarg.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/time.h>
#include "logger.h"

#define p_name "filter_module"

/*
 * timeout이 발생할 경우, pass된 이유에 대해서 로그를 남기기 위함.
 * timeout으로 인해 pass되기 전까지, call strace 정보 등을 저장.
 */
#define MAX_TIMEOUT_LOG_LEN     4096
static char timeout_log[MAX_TIMEOUT_LOG_LEN];
static struct timeval start, end;

#ifndef _RTLOG_

void log_init()
{
}

void log_clear()
{
}

void log_newline()
{
}

int log_append(const char *fmt, ...)
{
    return -1;
}

int log_cat(const char *fmt, ...)
{
    return -1;
}

int _log_cat(const char *fmt, va_list ap)
{
    return -1;
}

void log_write(const char *fmt, ...)
{
}

void log_flush()
{
}

void set_timeout_log( const char *add_log, const char *filename, const int file_size )
{
}

void add_timeout_log( const char *add_log )
{
}
void writeTimeOutLog( void )
{
}
#else

static const char *PREFIX = "FireWall :";
static const char *NEWLINE = "@n@";

void log_init()
{
    memset(log_buf, '\0', MAX_LOG_SIZE);

    strcat(log_buf, PREFIX);
    strcat(log_buf, NEWLINE);
    return;
}

void log_clear()
{
    log_init();
}

void log_newline()
{
    strcat(log_buf, NEWLINE);
}

int _log_cat(const char *fmt, va_list ap)
{

    char app_log[8096] = { '\0', };
    int app_log_size = 0;
    int log_buf_size = 0;

    vsprintf(app_log, fmt, ap);

    app_log_size = strlen(app_log);
    log_buf_size = strlen(log_buf);

    strncat(log_buf, app_log, app_log_size);

    return log_buf_size + app_log_size;
}

int log_cat(const char *fmt, ...)
{
    int ret = 0;
    va_list ap;

    va_start(ap, fmt);
    ret = _log_cat(fmt, ap);
    va_end(ap);

    return ret;
}

int log_append(const char *fmt, ...)
{
    int ret = 0;
    va_list ap;

    va_start(ap, fmt);
    ret = _log_cat(fmt, ap);
    va_end(ap);

    log_newline();

    return ret;
}
void log_write(const char *fmt, ...)
{
    char log[8096] = { '\0', };
    va_list ap;

    va_start(ap, fmt);
    vsprintf(log, fmt, ap);
    va_end(ap);

    loaderLog(L_DEBUG, "%s%s", PREFIX, log);
}

void log_flush()
{
    if (strlen(log_buf) > 0) {
        loaderLog(L_DEBUG, "%s", log_buf);
    }
}

void set_timeout_log( const char *add_log, const char *filename, const int file_size )
{
    char time_buf[1024] = { '\0', };

    memset( timeout_log, 0, MAX_TIMEOUT_LOG_LEN );
    memset( time_buf, 0, 1024 );
    get_time( time_buf );
    if( filename != NULL && file_size > 0 )
    {
        snprintf( timeout_log, MAX_TIMEOUT_LOG_LEN, "%s TIMEOUT FileSize : [%d], FileName : [%s]\n%s",
                time_buf, file_size, filename, add_log );
    }
    else
    {
        snprintf( timeout_log, MAX_TIMEOUT_LOG_LEN, "%s TIMEOUT File is not exist. File Size : [%d]\n%s", time_buf, file_size, add_log );
    }

    gettimeofday( &start, NULL );
}

void add_timeout_log( const char *add_log )
{
    char time_buf[1024];
    time_t tv_sec;
    suseconds_t tv_usec;
    int len;

    gettimeofday( &end, NULL );
    tv_sec = end.tv_sec - start.tv_sec;
    tv_usec = end.tv_usec - start.tv_usec;

    if( add_log != NULL )
        len = snprintf( time_buf, 1024, " : Second : [%ld] USecond : [%ld]\n%s", tv_sec, tv_usec, add_log );
    else
        len = snprintf( time_buf, 1024, " : Second : [%ld] USecond : [%ld]\n", tv_sec, tv_usec  );

    strncat( timeout_log, time_buf, len );
    gettimeofday( &start, NULL );
}
void writeTimeOutLog( void )
{
    int fd;
    int offset, wrote_size, len;

    len = strlen( timeout_log );
    fd = open( LOG_PATH, O_WRONLY );
    if( fd < 0 )
        return;

    lseek( fd, 0, SEEK_END );
    offset = wrote_size = 0;
    do
    {
        offset = write( fd, timeout_log + wrote_size, len - wrote_size );
        wrote_size += offset;
    } while( wrote_size < len );
    close( fd );

 	loaderLog( L_DEBUG, "%s", timeout_log );
}

#endif
