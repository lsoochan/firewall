#include <stdio.h>
#include <tre/regex.h>
#include <string.h>

#include "logger.h"
#include "common.h"

#define p_name "filter_module"

#define  FILTER_PASS    0
#define  FILTER_BLOCK   1

#define  MAX_REGEX_MATCH_STR           1024


int keyword_cnt = 0;
wchar_t **keyword_list;

int pattern_cnt = 0;
regex_t **pattern_list;

/**
* @author       sclee
* @date         2014-07-04
* @remark       키워드 필터 초기화 함수
* @param        keywords : 필터링 할 키워드 리스트
*               keywords_cnt : 키워드 개수
*				patterns : 필터링 할 정규표현식 리스트
*				patterns_cnt : 정규표현식 개수
* @return       없음
*/

void init_keywords(wchar_t **keywords, int keywords_cnt, regex_t **patterns, int patterns_cnt)
{
	keyword_cnt = keywords_cnt;
	keyword_list = keywords;

	pattern_list = patterns;
	pattern_cnt = patterns_cnt;
}

/**
* @author       sclee
* @date         2014-07-04
* @remark       각각의 키워드 존재 여부 확인
* @param        field : 스트림
*               keyword : 키워드
* @return       키워드 존재 여부
*/

static int filter_wkeyword(wchar_t *field, wchar_t *keyword)
{
	if (NULL == field || NULL == keyword) {
		return (FILTER_PASS);
	}

	if (wcslen(field) < wcslen(keyword))
        return (FILTER_PASS);

    if (wcsstr(field, keyword) == NULL)
        return (FILTER_PASS);
    else
        return (FILTER_BLOCK);

}

/**
* @author       sclee
* @date         2014-07-04
* @remark       각각의 정규표현식 패턴 비교
* @param        field : 스트림
*               pattern : 정규표현식 패턴
* @return       패턴 존재 여부
*/

unsigned long filter_pattern(wchar_t *field, regex_t *pattern)
{
	unsigned long pass = FILTER_PASS;
	int substr_len;
	regmatch_t result;

	if (0 == regwexec(pattern, field, 1, &result, 0)) {
        substr_len = result.rm_eo - result.rm_so;

        if (substr_len <= 0)
            return (pass);

        if (MAX_REGEX_MATCH_STR < substr_len)
            substr_len = MAX_REGEX_MATCH_STR;

        loaderLog(L_DEBUG,
                "filter_pattern: so = %d, eo = %d, strlen = %d",
                result.rm_so, result.rm_eo, substr_len);
        //wcsncpy(regex_matched_wstr, field + result.rm_so, substr_len);
        //regex_matched_wstr[substr_len] = '\0';
      	pass = (FILTER_BLOCK);
    }

    return (pass);
	
}

/**
* @author       sclee
* @date         2014-07-04
* @remark       정규표현식 필터링 함수
* @param        src : 스트림
* @return       정규표현식 검출 여부
*/

unsigned long filter_regex(wchar_t *src)
{
	int match_cnt = 0;
	int i;
	unsigned long pass;

	if (pattern_cnt == 0)
		return (0);

	for (i = 0; i < pattern_cnt; i++) {
		pass = filter_pattern(src, pattern_list[i]);
		if (FILTER_BLOCK == pass)
			match_cnt ++;
	}

	if (0 < match_cnt)
		return ((unsigned long)REQ__BLOCK_KEYWORD);
	else
		return (0);
}

/**
* @author       sclee
* @date         2014-07-04
* @remark       키워드 필터링 함수
* @param        src : 스트림
* @return       키워드 검출 여부
*/

unsigned long filter_keyword(wchar_t *src)
{
	int i;
	unsigned long pass;
	int match_cnt = 0;
	
	if (keyword_cnt == 0)
		return (0);
	
	for (i = 0; i < keyword_cnt; i++) {
		pass = filter_wkeyword(src, keyword_list[i]);
		if (FILTER_BLOCK == pass) {
			match_cnt ++;
		}
	}

	if (0 < match_cnt)
		return ((unsigned long)REQ__BLOCK_KEYWORD);
	return 0;
}

/**
* @author       sclee
* @date         2014-07-04
* @remark       개인정보 필터링 함수
* @param        src : 스트림
*               size : 스트림 길이
* @return       검출 여부
*/

unsigned long filter_privacy(char *src, int size) 
{
	int ret;
	char regex[256] = {0x00,};

	strcpy(regex, "([01][0-9]{5}[[:space:],~-]+[1-4][0-9]{6}|[2-9][0-9]{5}[[:space:],~-]+[1-2][0-9]{6})");

	loaderLog(L_DEBUG, "filter_privacy() : start");
	regex_t ext_regex;
	ret = regcomp(&ext_regex, regex, REG_EXTENDED);

	if (ret != 0) {
		loaderLog(L_ERR, "filter_privacy() : regcomp fails");
	}

	ret = regexec(&ext_regex, src, 0, NULL, 0);
	regfree(&ext_regex);
	return (!ret);
}
