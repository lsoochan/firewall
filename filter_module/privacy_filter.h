#include <stdio.h>

unsigned long filter_privacy(char *src, int size);
void init_keywords(wchar_t **keywords, int keywords_cnt, regex_t **patterns, int patterns_cnt);
unsigned long filter_keyword(wchar_t *src);
unsigned long filter_regex(wchar_t *src);
