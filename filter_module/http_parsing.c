#define  _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <iconv.h>

#include "logger.h"
#include "common.h"
#include "http_parsing.h"

//#define DEBUG

#define p_name "loader"

#define FIELD		0
#define VALUE		1

#define BOUNDARY_LEN_LIMIT 1024
#define LIST_SIZE_LIMIT 2048
#define FILE_PATH_LIMIT 2048
#define UTF16 0

struct _http_header {
	char *url;
	char *extra_url;
	char *host;
	char *cookie;
	char *referer;
	char *boundary;
	int content_type;
	int content_length;
	int url_len;
	int extra_url_len;
	int host_len;
	int cookie_len;
	int referer_len;
	int boundary_len;
};

static int parsing_http(char *dst, char *src,  u_int32_t * cnt,
			struct _http_header *header);
static char *get_url(char *target, struct _http_header *header);
static char *get_host(char *target, struct _http_header *header);
static int get_content_type(char *target, struct _http_header *header);
static char *get_cookie(char *target, struct _http_header *header);
static char *get_referer(char *target, struct _http_header *header);
static void what_content_type(char *start, struct _http_header *header);
static int get_content_length(char *target, struct _http_header *header);
static int parsing_http_body(char *dst, char *data, int data_size,
			     struct _http_header *header, u_int32_t * cnt);
static int parse_multipart(char *dst, char *data, int data_size,
			   struct _http_header *header, u_int32_t * cnt);
static int url_decode(char *dst, char *src, int data_size);
static int parse_url_encoded(char *dst, char *data, int data_size,
			     u_int32_t * cnt);
static int parse_xml(char *dst, char *data, int data_size, u_int32_t * cnt);
static int parse_extra_url(char *dst, char *data, int data_size,
			   u_int32_t * cnt);
#if 0
static int parse_text_plain(char *dst, char *data, int data_size,
			    u_int32_t * cnt);
#endif
static inline unsigned char my_tolower(unsigned char c);
static int my_strncasecmp(const char *s1, const char *s2, int n);


static const char *CON_DIS1 = "Content-Disposition: form-data; name=\"";
static const char *CON_DIS2 = "Content-disposition: form-data; name=\"";
static const char *CON_DIS3 = "content-disposition: form-data; name=\"";
static const int CON_DIS_SIZE = 38;

static const char *CON_TYPE1 = "Content-Type:";
static const char *CON_TYPE2 = "Content-type:";
static const char *CON_TYPE3 = "content-type:";
static const int CON_TYPE_SIZE = 13;

static const char *CON_FILENAME = "; filename=\"";
static const int CON_FILENAME_SIZE = 12;

static const char *CON_DATA = "\r\n\r\n";
static const int CON_DATA_SIZE = 4;

/**
* @author       sclee
* @date         2014-07-04
* @remark     	HTTP 헤더 구조체 초기화 함수
* @param        header : HTTP 헤더
* @return       없음
*/

static inline void init_http_header(struct _http_header *header)
{
	header->url = NULL;
	header->host = NULL;
	header->referer = NULL;
	header->boundary = NULL;
	header->content_type = 0;
	header->content_length = 0;
	header->url_len = 0;
	header->host_len = 0;
	header->referer_len = 0;
	header->boundary_len = 0;
}

/**
* @author       sclee
* @date         2014-07-04
* @remark       패킷 종류 판별 함수
* @param        p : 패킷 스트림
*               size : 패킷 스트림 길이
* @return       HTTP_Request : HTTP 요청 TYPE
*/

static enum HTTP_Request whatMethod(const char *p, int size)
{
	enum HTTP_Request request = REQUEST_UNKNOWN;
    if (NULL == p) {
        //Warning(("whatMethod() : p is NULL "));
        return (REQUEST_UNKNOWN);
    }
    if (size < 4) {     /* 4 = 3( the size of GET or PUT) + 1 (blank)  */
        return (REQUEST_UNKNOWN);
    }

    if (!strncmp(p, "GET ", 4)) {
        request = GET;
    } else if (!strncmp(p, "POST ", 5)) {
        request = POST;
    } else if (!strncmp(p, "PUT ", 4)) {
        request = PUT;
    } else if (!strncmp(p, "DELETE ", 7)) {
        request = DELETE;
    } else if (!strncmp(p, "HEAD ", 5)) {
        request = HEAD;
    } else if (!strncmp(p, "TRACE ", 6)) {
        request = TRACE;
    } else if (!strncmp(p, "OPTIONS ", 8)) {
        request = OPTIONS;
    } else if (!strncmp(p, "CONNECT ", 8)) {
        request = CONNECT;
    }

    return (request);	
}

enum HTTP_Request whatHTTPRequest(char *p)
{
	if (strlen(p) < 4)
		return (REQUEST_UNKNOWN);
	else
		return (whatMethod(p, strlen(p)));	
}

/**
* @author       sclee
* @date         2014-07-04
* @remark       HTML 태그 제거 함수
* @param        stream : 패킷 스트림
* @return       없음
*/

void remove_tags(char *stream)
{
    char *p;
    char *p1;
    char c;
    int depth;
    int num;
    int i;
        int stream_len = strlen(stream);
    p = (char *)stream;
    p1 = p;

    depth = 0;
    for (i = 0; i < stream_len; i++) {
        c = *(p + i);

        if ('<' == c)
            depth += 1;

        if (0 == depth) {
            *p1 = c;
            p1 += 1;
        }

        if (0 < depth && '>' == c)
            depth -= 1;
    }
    *p1 = '\0';

    stream_len =
        p1 - stream;

    p = (char *)stream;
    while (NULL != (p = strstr(p, "&"))) {
        if ((6 <= (p1 - p))
            && (0 == strncmp(p, "&nbsp;", 6)))
            num = 6;
        else if ((5 <= (p1 - p))
             && (0 == strncmp(p, "&amp;", 5)))
            num = 5;
        else if ((4 <= (p1 - p))
             && (0 == strncmp(p, "&gt;", 4)))
            num = 4;
        else if ((4 <= (p1 - p))
             && (0 == strncmp(p, "&lt;", 4)))
            num = 4;
        else
            num = 0;

        if (0 == num)
            p += 1;
        else {
            for (i = 0; i < num; i++)
                *(p + i) = ' ';

            p += num;
        }
    }
}
/* End of remove_tags() */
#if 0
int
http_parsing(struct _http_stream *http_stream, struct _http_tlv *http_tlv,
	     u_int64_t _id_64)
{
	struct _http_header header;
	char *stream = NULL;
	int ret;

	id_64 = _id_64;

#define http_copy( x ) 	http_tlv->x = http_stream->x;
	http_copy(tag);
	http_copy(http_id);
	http_copy(pass);
	http_copy(pstore_id);
	http_copy(method);
	http_copy(src_ip);
	http_copy(dst_ip);
	http_copy(src_port);
	http_copy(dst_port);
	http_copy(cnt);
	http_copy(transfer_type);
	http_copy(content_size);
#undef http_copy

	init_http_header(&header);
	loaderLog(LOG_FD, "HTTP Parsing Start!\n");
	if (unlikely(http_stream->transfer_type != HTTP_STREAM)) {
		stream = read_http_file(http_stream);
		if (stream == NULL)
			return PARSING_ERROR;
	} else
		stream = http_stream->stream;
	//loaderLog(LOG_FD,"HTTP_DATA = %s \n",stream);
	ret =
	    parsing_http(http_tlv->data, stream, http_stream->content_size,
			 &http_tlv->cnt, &header);

	http_tlv->url_len = header.host_len + header.url_len + 1;
	http_tlv->referer_len = header.referer_len + 1;
	//loaderLog(LOG_FD,"http_tlv->data = %s \n",http_tlv->data);
	if (HTTP_FILE == http_stream->transfer_type)
		free(stream);
	return (ret);
}
#endif

/**
* @author       sclee
* @date         2014-07-04
* @remark     	파싱 메인 함수
* @param        packet : 패킷 스트림
*               http_tlv : 스트림 파싱 정보 담을 구조체
*               size : 패킷 스트림 길이
* @return       파싱 성공 여부
*/

int http_parsing(char *packet, struct _http_tlv *http_tlv)
{
	struct _http_header header;
	int ret = 0;
	int request;

	request = whatHTTPRequest(packet);

	switch (request) {
		case POST:
			loaderLog(L_DEBUG, "http_parsing() : POST packet");
			ret = parsing_http(http_tlv->data , packet,  &http_tlv->cnt, &header);
			http_tlv->url_len = header.host_len + header.url_len + 1;
			http_tlv->referer_len = header.referer_len + 1;
			loaderLog(L_DEBUG, "http_parsing() : ret [%d]", ret);
			break;
		case GET:
			loaderLog(L_DEBUG, "http_parsing() : GET packet");
			break;
		default:
			loaderLog(L_DEBUG, "http_parsing() : UNKNOWN packet");
			break;
	}

	return ret;
}

/**
* @author       sclee
* @date         2014-07-04
* @remark       POST 패킷 파싱 함수
* @param        dst : 파싱 결과 담을 스트림
*				src : 패킷스트림
*               cnt : 필드 개수 담을 스트림
*               header : 헤더 구조체
* @return       없음
*/

static int
parsing_http(char *dst, char *src,  u_int32_t * cnt,
	     struct _http_header *header)
{
	int token = '\n';
	char *position = src;
	char *start_data;
	int find_url, find_host, find_content_type, find_referer,
	    find_content_length, find_cookie;
	int parsed_size_header = 0;
	int parsed_size_body = 0;

	loaderLog(L_DEBUG, "parsing_http() start");

	find_url = find_host = find_content_type = find_referer =
	    find_content_length = find_cookie = 0;

	if (dst == NULL || src == NULL)
		return PARSING_ERROR;
	loaderLog(L_DEBUG, "parsing_http() st1");
	while (1) {
		if ('\r' == position[0]) {
			start_data = position + 2;	/* 2 is "\r\n" */
			break;
		}
		if (0 == find_url && (NULL != get_url(position, header))) {
			find_url = 1;
		} else if (0 == find_host
			   && (get_host(position, header) != NULL)) {
			find_host = 1;
		} else if (0 == find_content_type
			   && (get_content_type(position, header) != 0)) {
			find_content_type = 1;
		} else if (0 == find_cookie
			   && (get_cookie(position, header) != NULL)) {
			find_cookie = 1;
		} else if (0 == find_referer
			   && (get_referer(position, header) != NULL)) {
			find_referer = 1;
		} else if (0 == find_content_length
			   && (get_content_length(position, header) > 0)) {
			find_content_length = 1;
		}

		position = strchr(position, token);
		if (NULL == position)
			break;

		position++;
	}
	loaderLog(L_DEBUG, "parsing_http() st2host_len : %d", header->host_len);
	// Host와 Url을 붙여서 입력한다. ex.) www.daum.net/input.php
	strncpy(dst, header->host, header->host_len);
	strncpy(dst + header->host_len, header->url, header->url_len);
	dst[header->host_len + header->url_len] = '\0';	/* 1 is '\0' */
	loaderLog(L_DEBUG, "parsing_http() dst : %s", dst);
	strncpy(dst + header->host_len + header->url_len + 1, header->referer,
		header->referer_len);
	dst[header->host_len + header->url_len + header->referer_len + 1] = '\0';	/* 1 is '\0' */

	/* TODO: host, url must save at first position */
	dst = dst + header->url_len + header->host_len + header->referer_len + 2;	/* 2 is '\0' */

	*cnt = 0;
	loaderLog(L_DEBUG, "parsing_http() st3");

	if (0 < header->extra_url_len && NULL != header->extra_url) {
		//loaderLog(L_DEBUG, "parse_extra_url() : Start~");
		parsed_size_header =
		    parse_extra_url(dst, header->extra_url,
				    header->extra_url_len, cnt);

		dst += parsed_size_header;
	}
	loaderLog(L_DEBUG, "parsing_http() st4");
	parsed_size_body =
	    parsing_http_body(dst, start_data, header->content_length, header,
			      cnt);
	if (0 != (parsed_size_header + parsed_size_body))
		return (PARSING_OK);
	else
		return (PARSING_ERROR);
}

/**
* @author       sclee
* @date         2014-07-04
* @remark       POST 패킷 URL 추출 함수
* @param        target : 스트림 
*               header : 결과 담을 헤더
* @return       URL 주소값
*/

static char *get_url(char *target, struct _http_header *header)
{
	char *delim = "POST ";
	int delim_len = strlen(delim);
	char temp;
	int i, j;
	loaderLog(L_DEBUG, "LSC - hih");
	if (0 != my_strncasecmp(target, delim, delim_len))
		return (NULL);
	loaderLog(L_DEBUG, "LSC - hih2");
	header->url = target + delim_len;
	for (i = 0; header->url[i] != '\r'; i++) {
		temp = header->url[i];
		if (temp == '?' || temp == ' ')
			break;
	}

	header->url_len = i;

	j = i + 1;
	header->extra_url = &(header->url[j]);
	for (i = 0; header->extra_url[i] != '\r'; i++) {
		if (!my_strncasecmp(&(header->extra_url[i]), "HTTP", 4)) {
			i -= 1;
			break;
		}
	}

	if (i <= 0) {
		header->extra_url = NULL;
		header->extra_url_len = 0;
	} else
		header->extra_url_len = i;

#ifdef DEBUG
	printf("Url = [");
	for (i = 0; i < header->url_len; i++)
		printf("%c", header->url[i]);
	printf("]\n");

	printf("Extra Url Len = [%d], [", header->extra_url_len);
	for (i = 0; i < header->extra_url_len; i++)
		printf("%c", header->extra_url[i]);
	printf("]\n");
#endif
	return header->url;
}

/**
* @author       sclee
* @date         2014-07-04
* @remark       패킷 HOST 추출 함수
* @param        target : 스트림
*               header : 결과 담을 헤더
* @return       HOST 주소값
*/

static char *get_host(char *target, struct _http_header *header)
{
	char *delim = "Host: ";
	int delim_len = strlen(delim);
	int i;
	loaderLog(L_DEBUG, "LSC - hi h3");
	if (my_strncasecmp(target, delim, delim_len))
		return NULL;
	loaderLog(L_DEBUG, "LSC - hi h4");
	header->host = target + delim_len;
	for (i = 0; header->host[i] != '\r'; i++)
		if (header->host[i] == ':')
			break;
	header->host_len = i;
	loaderLog(L_DEBUG, "LSC - have host");
#ifdef DEBUG
	printf("Host = [");
	for (i = 0; i < header->host_len; i++)
		printf("%c", header->host[i]);
	printf("]\n");
#endif
	return header->host;
}

/**
* @author       sclee
* @date         2014-07-04
* @remark       패킷 ContentTpye 추출 함수
* @param        target : 스트림
*               header : 결과 담을 헤더
* @return       컨텐트 타입
*/

static int get_content_type(char *target, struct _http_header *header)
{
	char *delim = "Content-Type: ";
	char *position;
	int delim_len = strlen(delim);
#ifdef DEBUG
	int i = 0;
#endif

	if (my_strncasecmp(target, delim, delim_len))
		return ENC_TYPE_UNKNOWN;

	position = target + delim_len;
	what_content_type(position, header);

#ifdef DEBUG
	printf("Enc Type : [%d]\n", header->content_type);
	if (header->content_type == MULTI_PART) {
		printf("Boundary = [");
		for (i = 0; i < header->boundary_len; i++)
			printf("%c", header->boundary[i]);
		printf("]\n");
	}
#endif
	return header->content_type;
}

/**
* @author       sclee
* @date         2014-07-04
* @remark       POST 패킷 content type 조사 함수
* @param        start : 스트림
*               header : 결과 담을 헤더
* @return       없음
*/

static void what_content_type(char *start, struct _http_header *header)
{
	char *multipart = "multipart/form-data";
	char *url_encoding = "application/x-www-form-urlencoded";
	char *text_plain = "text/plain";
	char *temp;
	int compare_size;

	compare_size = strlen(multipart);
	if (!my_strncasecmp(start, multipart, compare_size)) {
		int i;
		int boundary_len = strlen("boundary") + 1;	/* 1 is '=' of "boundary=" */

		temp = strstr(start, "boundary");
		if (temp == NULL) {
			header->content_type = ENC_TYPE_UNKNOWN;
			header->boundary = NULL;
			return;
		}
		header->content_type = MULTI_PART;
		header->boundary = temp + boundary_len;
		for (i = 0; header->boundary[i] != '\r'; i++) ;
		header->boundary_len = i;	/* 2 is "; " of "multipart/form-data; " and 1 is carrige return */
		return;
	}

	compare_size = strlen(url_encoding);
	if (!my_strncasecmp(start, url_encoding, compare_size)) {
		header->content_type = URL_ENCODED;
		return;
	}

	compare_size = strlen(text_plain);
	if (!my_strncasecmp(start, text_plain, compare_size)) {
		header->content_type = PLAIN_TEXT;
		return;
	}
	header->content_type = ENC_TYPE_UNKNOWN;
}

/**
* @author       sclee
* @date         2014-07-04
* @remark       패킷 쿠키 추출 함수
* @param        target : 스트림
*               header : 결과 담을 헤더
* @return       쿠키 주소값
*/

static char *get_cookie(char *target, struct _http_header *header)
{
	char *delim = "Cookie: ";
	int delim_len = strlen(delim);
	int i;

	if (my_strncasecmp(target, delim, delim_len))
		return NULL;

	header->cookie = target + delim_len;
	for (i = 0; header->cookie[i] != '\r'; i++) ;
	header->cookie_len = i;

#ifdef DEBUG
	printf("Cookie = [");
	for (i = 0; i < header->cookie_len; i++)
		printf("%c", header->cookie[i]);
	printf("]\n");
#endif
	return header->cookie;
}

/**
* @author       sclee
* @date         2014-07-04
* @remark       패킷 referer 추출 함수
* @param        target : 스트림
*               header : 결과 담을 헤더
* @return       referer 주소값
*/

static char *get_referer(char *target, struct _http_header *header)
{
	char *delim = "Referer: ";
	int delim_len = strlen(delim);
	int i;

	if (my_strncasecmp(target, delim, delim_len))
		return NULL;

	header->referer = target + delim_len;
	for (i = 0; header->referer[i] != '\r'; i++) ;
	header->referer_len = i;	/* 1 is carrige return */

#ifdef DEBUG
	printf("Referer = [");
	for (i = 0; i < header->referer_len; i++)
		printf("%c", header->referer[i]);
	printf("]\n");
#endif
	return header->referer;
}

/**
* @author       sclee
* @date         2014-07-04
* @remark       패킷 content length 추출 함수
* @param        target : 스트림
*               header : 결과 담을 헤더
* @return       content length
*/

static int get_content_length(char *target, struct _http_header *header)
{
	char *delim = "Content-Length: ";
	int delim_len = strlen(delim);

	if (my_strncasecmp(target, delim, delim_len))
		return -1;

	header->content_length = atoi(target + delim_len);
#ifdef DEBUG
	printf("Content Length = [ %d ]\n", header->content_length);
#endif
	return header->content_length;
}

static int
parse_extra_url(char *dst, char *data, int data_size, u_int32_t * cnt)
{
	return parse_url_encoded(dst, data, data_size, cnt);
}

/**
* @author       sclee
* @date         2014-07-04
* @remark       POST 패킷 BODY 파싱 함수
* @param        dst : 결과 스트림
*				data : 스트림
*				data_size : 스트림 길이
*               header : 결과 담을 헤더
*				cnt : 필드 개수
* @return       성공여부
*/

static int
parsing_http_body(char *dst, char *data, int data_size,
		  struct _http_header *header, u_int32_t * cnt)
{
	char *pos_data = dst;
	int parse_size = 0;
	
	if (0 == data_size)
		return (0);
	switch (header->content_type) {
	case MULTI_PART:
		parse_size =
		    parse_multipart(pos_data, data, data_size, header, cnt);
		break;

	case URL_ENCODED:
		parse_size = parse_url_encoded(pos_data, data, data_size, cnt);
		break;
/*
	case PLAIN_TEXT:
		parse_size = parse_text_plain(pos_data, data, data_size, cnt);
		break;
*/
	default:
		if (strstr(data, "<?xml") != NULL)
			parse_size = parse_xml(pos_data, data, data_size, cnt);
		else {
			//loaderLog(L_DEBUG,
			///	  "parsing_http_body() : invalid content_type [%d]",
			//		header->content_type);
		}
	}

	return (parse_size);
}


/**
* @author       sclee
* @date         2014-07-04
* @remark       POST 패킷 Multipart 파싱 함수
* @param        dst : 결과 스트림
*				data : 원본 스트림
*				data_size : 원본 스트림 길이
*               header : 결과 담을 헤더
*				cnt : 필드 개수
* @return       없음
*/

static int
parse_multipart(char *dst, char *data, int data_size,
		struct _http_header *header, u_int32_t * cnt)
{
	int i, j;

	unsigned int offset = 0;

	char *log_buf = dst;
	int curr_field_size = 0;
	int sum_field_size = 0;

	struct _field *field = NULL;
	struct _file *file = NULL;

	int b_len;
	char boundary[BOUNDARY_LEN_LIMIT];

	char *field_list[LIST_SIZE_LIMIT] = { '\0', };
	int field_size[LIST_SIZE_LIMIT] = { 0, };
	int field_cnt = 0;

	char afilepath[FILE_PATH_LIMIT + 1] = { '\0', };
	char origfilepath[FILE_PATH_LIMIT + 1] = { '\0', };

	char *next_boundary = NULL;
	char *con_dis = NULL;
	char *con_type = NULL;
	char *con_filename = NULL;
	char *con_data = NULL;

	char *data_buf = data;
	size_t data_buf_size = data_size;
	char *cp = NULL;
	int file_cnt = 0;

	if (header->boundary == NULL) {
		//loaderLog(L_DEBUG, "POST header doesn't contain boundary.");
		goto malformed;
	}
	if (data == NULL || dst == NULL
	    || header->boundary_len > BOUNDARY_LEN_LIMIT) {
		//loaderLog(L_DEBUG,
		//	  "parse_multipart:  dst == NULL or dst == NULL or header->boundary_len > %d",
		//	  BOUNDARY_LEN_LIMIT);
		goto malformed;

	}

	boundary[0] = '\r';
	boundary[1] = '\n';
	boundary[2] = '-';
	boundary[3] = '-';

	strncpy(boundary + 4, header->boundary,
		(header->boundary_len <
		 BOUNDARY_LEN_LIMIT) ? header->
		boundary_len : BOUNDARY_LEN_LIMIT - 4);
	b_len = header->boundary_len + 4;

	boundary[b_len] = '\0';

	field_cnt = 0;

	// 멀티 파트에서 첫번째 바운더리는 \r\n으로 시작하지 않는다. 사실 두번째 바운더리도 \r\n으로 "시작" 하는것은 아니지만 그 전 바운더리의 마지막이 \r\n으로 끝나므로
	// \r\n으로 시작하는 바운더리를 찾도록 하였기 때문에 맨 처음 바운더리에 대해서는 예외적으로 \r\n을 붙이지 않고 검색한다.
	if ((cp =
	     memmem(data_buf, data_buf_size, boundary + 2,
		    b_len - 2)) != NULL) {
		//첫번째 바운더리를 찾았음.
		field_list[field_cnt++] = cp;
		data_buf_size = data_buf_size - (cp - data_buf) - (b_len - 2);
		//첫번째 바운더리의 시작 포인터를data_buf에 넣음.
		data_buf = cp + (b_len - 2);

		while ((cp =
			memmem(data_buf, data_buf_size, boundary,
			       b_len)) != NULL) {
			field_size[field_cnt - 1] =
			    //(unsigned int)cp -
			    //(unsigned int)(field_list[field_cnt - 1]);
				cp - (field_list[field_cnt - 1]);
			field_list[field_cnt++] = cp;
			data_buf_size = data_buf_size - (cp - data_buf) - b_len;
			data_buf = cp + b_len;

			//현재까지 발견된 바운더리가 LIST_SIZE_LIMIT 보다 큰 경우 더 이상 처리하지 않음.
			if (field_cnt >= LIST_SIZE_LIMIT) {
				//loaderLog(L_DEBUG,
				//	  "multipart data has more than LIST_SIZE_LIMIT(%d) boundary sections.",
				//	  LIST_SIZE_LIMIT);
				break;
			}
		}
	}

	if (0 == field_cnt) {
		//loaderLog(L_ERR, "Couldn't find boundary in data = [%s]",
		//	  boundary);
		goto malformed;
	}

	for (i = 0; i < field_cnt - 1; i++) {
		next_boundary = field_list[i + 1];
		con_dis = NULL;
		con_type = NULL;
		con_filename = NULL;
		
		//loaderLog(LOG_FD,"%X %X %X %X \n",field_list[i],field_list[i+1],field_list[i+2],field_list[i+3]);
		//FILENAME 필드를 찾음. COND_DATA는 \r\n으로  
		con_filename =
		    memmem(field_list[i], field_size[i], CON_FILENAME,
			   CON_FILENAME_SIZE);
		con_data =
		    memmem(field_list[i], field_size[i], CON_DATA,
			   CON_DATA_SIZE);

		con_dis =
		    memmem(field_list[i], field_size[i], CON_DIS1,
			   CON_DIS_SIZE);
		if (NULL == con_dis) {
			con_dis =
			    memmem(field_list[i], field_size[i], CON_DIS2,
				   CON_DIS_SIZE);
			if (NULL == con_dis)
				con_dis =
				    memmem(field_list[i], field_size[i],
					   CON_DIS3, CON_DIS_SIZE);
		}

		con_type =
		    memmem(field_list[i], field_size[i], CON_TYPE1,
			   CON_TYPE_SIZE);
		if (NULL == con_type) {
			con_type =
			    memmem(field_list[i], field_size[i], CON_TYPE2,
				   CON_TYPE_SIZE);
			if (NULL == con_type)
				con_type =
				    memmem(field_list[i], field_size[i],
					   CON_TYPE3, CON_TYPE_SIZE);
		}

		if (con_dis == NULL) {
			//loaderLog(L_ERR,
			//	  "parse_multipart() : condis == NULL ");
			goto malformed;
		}

		if (con_data == NULL) {
			//loaderLog(L_ERR,
			//	  "parse_multipart() : con_data == NULL ");
			goto malformed;
		}
		// Compare memory address whether each con_dis, con_... belongs to this boundary section.
		if (con_dis > next_boundary) {
			//loaderLog(L_ERR,
			//	  "parse_multipart() : con_dis > next_boundary ");
			goto malformed;
		}

		if (con_data > next_boundary) {
			//loaderLog(L_ERR,
			//	  "parse_multipart() : con_data > next_boundary ");
			goto malformed;
		}

		if (con_filename > next_boundary) {
			//loaderLog(L_ERR,
			//	  "parse_multipart() : con_filename > next_boundary ");
			con_filename = NULL;
		}

		if (con_type > next_boundary) {
			//loaderLog(L_ERR,
			//	  "parse_multipart() : con_type > next_boundary ");
			con_type = NULL;
		}
		// If it is not a file field or 
		// treat file field without filename(con_filename[0] == '\"') as normal field.  
		// 현재 바운더리가 파일 필드가 아니거나 파일 필드라도 파일이름이 없는 경우 일반 필드로 인식하여 처리.
		if (con_filename == NULL
		    || (con_filename != NULL && con_filename[0] == '\"')) {
			field = (struct _field *)log_buf;
			memset(field, 0, sizeof(struct _field));
			field->kind = KIND__FIELD;

			con_dis += CON_DIS_SIZE;

			for (j = 0;
			     (con_dis + j < next_boundary)
			     && (con_dis[j] != '\"'); j++)
				field->data[field->name_len++] = con_dis[j];

			if (field->name_len > 0)
				field->data[field->name_len++] = '\0';
			else {
				//loaderLog(L_ERR,
				//	  "parse_multipart() : field->name_len <= 0 ");
				continue;
				//goto malformed;
			}

			offset = field->name_len;

			con_data += CON_DATA_SIZE;
			for (j = 0; con_data + j < next_boundary; j++) {
				field->data[offset + field->value_len++] =
				    con_data[j];
			}

			field->data[offset + field->value_len++] = '\0';

			*cnt = *cnt + 1;
			curr_field_size =
			    sizeof(field->kind) + sizeof(field->name_len) +
			    sizeof(field->value_len);
			curr_field_size += field->name_len;
			curr_field_size += field->value_len;

			log_buf += curr_field_size;
			sum_field_size += curr_field_size;

#if 0
			loaderLog(LOG_FD, "field -> |%d|%d|%s|%.50s|",
				  field->name_len, field->value_len,
				  field->data, field->data + field->name_len);
#endif

		} else {
			//현재 바운더리가 파일일 경우.
			file = (struct _file *)log_buf;
			memset(file, 0, sizeof(struct _file));
			file->kind = KIND__FILE;

			//FIXME: file type won't be used. Depreacated field. Remove this field here and db if you want to.
			file->type = 1;

			con_dis = con_dis + CON_DIS_SIZE;

			for (j = 0;
			     (con_dis + j < next_boundary)
			     && (con_dis[j] != '\"'); j++)
				file->data[file->name_len++] = con_dis[j];

			if (0 < file->name_len)
				file->data[file->name_len++] = '\0';
			else {
				//loaderLog(L_ERR,
				//	  "parse_multipart() : file->name_len <= 0 ");
				goto malformed;
			}

			offset = file->name_len;
			con_filename += CON_FILENAME_SIZE;

			for (j = 0;
			     (con_filename + j < next_boundary)
			     && (con_filename[j] != '\"')
			     && j < FILE_PATH_LIMIT; j++) {
				origfilepath[j] = con_filename[j];
			}
			origfilepath[j] = '\0';

			con_data += CON_DATA_SIZE;

			file->size = next_boundary - con_data;
			if (0 == file->size) {
				if ('\0' != origfilepath[0])
					//loaderLog(L_ERR,
					//	  "The ize of an attached file is zero. filename = [%s]",
					//	  origfilepath);

				continue;
			}
#if 1 /* sclee */
			strcpy(afilepath, "dummey");
#else
			get_afile_path(afilepath, FILE_PATH_LIMIT, id_64,
				       file_cnt, get_file_ext(origfilepath));
#endif

#if 0	// 파일 제거. 사이냅 Memory Library를 사용
			if (write_afile(afilepath, file->size, con_data) == -1) {
				loaderLog(LOG_FD,
					  "Extracting file from post data is failed. Ignore the file.");
				continue;
			} else {
#endif
			for (j = 0; j < strlen(afilepath); j++)
				file->data[offset + file->fname_len++] =
					afilepath[j];
			file->data[offset + file->fname_len++] = '\0';
			offset += file->fname_len;

			for (j = 0; j < strlen(origfilepath); j++)
				file->data[offset + file->path_len++] =
					origfilepath[j];
			file->data[offset + file->path_len++] = '\0';

			offset += file->path_len;
			memcpy( file->data + offset, con_data, file->size );
			file_cnt++;
//			}
			*cnt = *cnt + 1;
			curr_field_size = 
			    sizeof(file->kind) + sizeof(file->type) +
			    sizeof(file->size) + sizeof(file->name_len) +
			    sizeof(file->fname_len) + sizeof(file->path_len);
			curr_field_size += file->name_len;
			curr_field_size += file->fname_len;
			curr_field_size += file->path_len;
			curr_field_size += file->size;

			log_buf += curr_field_size;
			sum_field_size += curr_field_size;

#if 0
			loaderLog(LOG_FD, "Multipart, file -> |%s|%s|%s|",
				  file->data, file->data + file->name_len,
				  file->data + file->name_len +
				  file->fname_len);
#endif
		}
	}

	return (sum_field_size);

 malformed:

	//loaderLog(L_ERR, "Malformed post multipart data.");
	return (PARSING_ERROR);
}


static int url_decode(char *dst, char *src, int data_size)
{
	int i, j;
	int num_except = 0;

	if (dst == NULL || src == NULL) {
		//loaderLog(L_ERR, "Input Pointer is Null!\n");
		return PARSING_ERROR;
	}
	for (i = 0, j = 0; i < data_size; i++, j++) {
		if (src[i] == '+')
			dst[j] = ' ';
		else if ((src[i] == '%') && isxdigit(src[i + 1])
			 && isxdigit(src[i + 2])) {
			char temp = src[i + 1];

			dst[j] =
			    ((temp >= '0'
			      && temp <=
			      '9') ? (temp - '0') : ((char)toupper(temp) - 'A' +
						     10)) << 4;
			temp = src[i + 2];
			dst[j] |=
			    ((temp >= '0'
			      && temp <=
			      '9') ? (temp - '0') : ((char)toupper(temp) - 'A' +
						     10));
			num_except += 2;
			i += 2;
		} else
			dst[j] = src[i];
	}

	return (data_size - num_except);
}
#if UTF16 
static int url_decode_utf16(char *dst, char *src, int data_size)
{
	int i, j, k;
	if( dst == NULL || src == NULL )
	{
		return -1;
	}

	for( i = 0, j = 0; i < data_size; i++ )
	{
		if( src[i] == '+' )
			dst[j++] = ' ';
		else if( (src[i] == '%') && (src[i+1]=='u') && isxdigit(src[i+2]) && isxdigit(src[i+3]) && isxdigit(src[i+4]) && isxdigit(src[i+5]) )
		{
			char temp = src[i+2];
			char text[2];
			dst[j+1] = ( (temp >= '0' && temp <= '9') ? (temp - '0') : ( (char)toupper(temp) - 'A' + 10) ) << 4;
			temp = src[i+3];
			dst[j+1] |= ( (temp >= '0' && temp <= '9') ? (temp - '0') : ( (char)toupper(temp) - 'A' + 10) );
			text[1] = dst[j+1];
			temp = src[i+4];
			dst[j] = ( (temp >= '0' && temp <= '9') ? (temp - '0') : ( (char)toupper(temp) - 'A' + 10) ) << 4;
			temp = src[i+5];
			dst[j] |= ( (temp >= '0' && temp <= '9') ? (temp - '0') : ( (char)toupper(temp) - 'A' + 10) );
			text[0] = dst[j];
			int ret;
			char *pdst = text;
			size_t pdst_size = 2;
			iconv_t iconv_fd;
			char output[8];
			size_t pout_size = 8;
			iconv_fd = iconv_open(  "UTF-8", "UTF-16" );
			if( iconv_fd < 0 )
			{
				return -1;
			}
			char *pout = output;
			ret = iconv( iconv_fd, &(pdst), &(pdst_size), &pout, &pout_size );
			for(k = 0; k < pout_size; k++ )
			{
				dst[j+k] = output[k];
			}
			iconv_close(iconv_fd);
			i += 5;
			j += pout_size - 2;
		}
		else if( (src[i] == '%') )
		{
			if( (src[i+1] == '0') && (src[i+2] == 'A' ) )
			{
				dst[j] = '\r';
				i += 2;
				j += 1;
			}
			else if( (src[i+1] == '2') && (src[i+2] == '0') )
			{
				dst[j] = ' ';
				i += 2;
				j += 1;
			}
			else if( (src[i+1] == '0') && (src[i+2] == 'D') && (src[i+3] == '%') && (src[i+4] == '0') && (src[i+5] == 'A') )
            {
                dst[j] = '\r';
                i += 5;
                j += 1;
            }

		}
		else
			dst[j++] = src[i];
	}
	return j;
}
#endif
static int
parse_url_encoded(char *dst, char *data, int data_size, u_int32_t * cnt)
{
	int i, j;
	char *log_buf = dst;
	struct _field *field;
	int last_field_size = 0;
	int sum_field_size = 0;

	int start_position = 0;

	//loaderLog(L_DEBUG, "URL encoded parsing start~");
	for (i = 0; i < data_size; i++) {
		log_buf += last_field_size;
		field = (struct _field *)log_buf;
		field->name_len = 0;
		field->value_len = 0;

		if (data[i] == '?' || data[i] == '&' || i == 0) {
			field->kind = KIND__FIELD;

			while (data[i] == '?' || data[i] == '&'
			       || data[i] == '\r' || data[i] == '\n') {
				i++;

				if (i >= data_size) {
					return sum_field_size;
				}
			}

			start_position = i;
			for (j = start_position; j < data_size; j++) {
				if ('=' == data[j] || '&' == data[j])
					break;
			}

			field->name_len =
			    url_decode(field->data, data + start_position,
				       j - start_position);

			/*
			   if(field->name_len <= 0)
			   return PARSING_ERROR;
			 */

			field->data[field->name_len++] = '\0';

			if ('=' == data[j])
				start_position = j + 1;
			else
				start_position = j;

			for (j = start_position; j < data_size; j++) {
				if (data[j] == '&')
					break;
			}
#if UTF16
			field->value_len =
				url_decode_utf16(field->data + field->name_len,
						data + start_position,
						j - start_position);
#else
			field->value_len =
			    url_decode(field->data + field->name_len,
				       data + start_position,
				       j - start_position);
#endif
			field->data[field->name_len + field->value_len] = '\0';
			field->value_len++;

			*cnt = *cnt + 1;
			last_field_size =
			    sizeof(field->kind) + sizeof(field->name_len) +
			    sizeof(field->value_len);
			last_field_size += field->name_len;
			last_field_size += field->value_len;
			sum_field_size += last_field_size;

#if 0
			loaderLog(LOG_FD, "field -> |%d|%d|%s|%.50s|",
				  field->name_len, field->value_len,
				  field->data, field->data + field->name_len);
#endif
		}

		i = j - 1;
	}

	return sum_field_size;
}
#if 0
static int
parse_text_plain(char *dst, char *data, int data_size, u_int32_t * cnt)
{
	return PARSING_ERROR;
}
#endif

/**
* @author       sclee
* @date         2014-07-04
* @remark       POST 패킷 XML 파싱 함수
* @param        dst : 결과 스트림
*				data : 원본 스트림
*				data_size : 원본 스트림 길이
*               header : 결과 담을 헤더
*				cnt : 필드 기수
* @return       파싱 성공 여부
*/


static int parse_xml(char *dst, char *data, int data_size, u_int32_t * cnt)
{
	int i;
	char *log_buf = dst;
	struct _field *field;
	int last_field_size = 0;
	int sum_field_size = 0;
	char *start;
	char *end;
	if ((start = strstr(data, "<params>")) != NULL) {
		start = strchr(start, '\n') + 1;
		end = strstr(data, "</params>");
		end = end - 2;
		if ((end - start) < 1)
			return PARSING_ERROR;
	} else {
		return PARSING_ERROR;
	}

	//loaderLog(L_DEBUG, "xml parsing start~");
	while (start < end) {
		if (*start == '<') {
			log_buf += last_field_size;
			field = (struct _field *)log_buf;
			field->name_len = 0;
			field->value_len = 0;
			field->kind = KIND__FIELD;
			start += 1;
			i = 0;
			while ((start + i) < end) {
				if (start[i] == '>')
					break;
				else
					i++;
			}
			if ((start + i) == end)
				return sum_field_size;

			strncpy(field->data, start, i);
			field->name_len = i;
			field->data[field->name_len++] = '\0';
			start = strstr(start, "CDATA[");
			if (start != NULL) {
				start += 6;
				if (start >= end)
					return sum_field_size;
			} else
				return sum_field_size;

			i = 0;
			while ((start + i) < end) {
				if (start[i] == ']')
					break;
				else
					i++;
			}

			if ((start + i) == end)
				return sum_field_size;

			strncpy(field->data + field->name_len, start, i);
			field->value_len = i;
			field->data[field->name_len + field->value_len] = '\0';
			field->value_len++;

			*cnt = *cnt + 1;
			last_field_size =
			    sizeof(field->kind) + sizeof(field->name_len) +
			    sizeof(field->value_len);
			last_field_size += field->name_len;
			last_field_size += field->value_len;
			sum_field_size += last_field_size;

#if 0
			loaderLog(LOG_FD, "field -> |%d|%d|%s|%.50s|",
				  field->name_len, field->value_len,
				  field->data, field->data + field->name_len);
#endif

			start = strstr(start, field->data);
			if (start != NULL) {
				start = start + field->name_len;
				if (start >= end)
					return sum_field_size;
			} else
				return sum_field_size;
		} else
			start += 1;

	}

	return sum_field_size;
}

/**
* @author       sclee
* @date         2014-07-04
* @remark       알파벳 소문자화 함수
* @param        c : 알파벳
* @return       소문자화 된 알파벳
*/

static inline unsigned char my_tolower(unsigned char c)
{
	if ('A' <= c && c <= 'Z')
		return (c - ('A' - 'a'));
	else
		return (c);
}

/**
* @author       sclee
* @date         2014-07-04
* @remark       대소문자 구분 없는 strncmp 함수
* @param        s1, s2 : 비교 스트림
*               n : 비교 할 size
* @return       일치 여부
*/

static int my_strncasecmp(const char *s1, const char *s2, int n)
{
	int c1;
	int c2;

	do {
		c1 = my_tolower(*s1++);
		c2 = my_tolower(*s2++);
	} while ((0 < --n) && c1 == c2 && 0 != c1);

	return (c1 - c2);
}
