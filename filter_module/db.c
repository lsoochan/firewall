#include <syslog.h>
#include <stdarg.h>
//#include <postgresql/libpq-fe.h>

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>


#include "db.h"
#include "common.h"
#include "logger.h"

#define p_name "postgresql"

static int DB_RESULT_CNT = 0;
static PGconn *conn = NULL;
static char sql_buf[SQL_BUF_LEN];
static int dbstatno;

pthread_mutex_t lock_db = PTHREAD_MUTEX_INITIALIZER;

/**
* @author       sclee
* @date         2014-07-04
* @remark     	DB 초기화 및 접속 함수
* @param        db_host : 접속할 DB 서버 IP
*               db_port : 접속할 DB PORT
* @return       일치 여부
*/

int dbinit(char *db_host, char *db_port)
{
    char conninfo[SMALL_BUF_LEN];

    sprintf(conninfo, "hostaddr=%s dbname=%s port=%s", db_host, DB_NAME,
        db_port);
    loaderLog( L_DEBUG, "DB connection = [%s] ", conninfo ) ;

    conn = PQconnectdb(conninfo);

    if (conn == NULL) {
	 loaderLog( L_DEBUG, "conn null");
        setstatus(DB_CONN_FAIL);
        return 0;
    }

#if 0
    if (!dbconnok()) {
	 loaderLog( L_DEBUG, "conn err");
        setstatus(DB_CONN_ERR);
        return 0;
    }
#endif
    setstatus(DB_CONN_OK);

    return 1;
}

int setstatus(int statno)
{
    dbstatno = statno;
    return 1;
}

/**
* @author       sclee
* @date         2014-07-04
* @remark      	DB 상태 체크 함수
* @param        
* @return       상태 번호
*/

int dbstatus()
{
    switch (dbstatno) {
    case DB_QUERY_OK:
        //loaderLog(LOG_FD, "Query executed successfully");
        break;
    case DB_QUERY_FAIL:
        //loaderLog(LOG_FD, "Query failed to be excuted");
        break;
    case DB_CONN_OK:
        //loaderLog(LOG_FD, "DB CONNECTION OK");
        break;
    case DB_CONN_FAIL:
        //loaderLog(LOG_FD, "PQconnectdb() failed.");
        break;
    case DB_CONN_ERR:
        //loaderLog(LOG_FD, "PQstatus() != CONNECTION_OK");
        break;
    }

    return dbstatno;
}

/**
* @author       sclee
* @date         2014-07-04
* @remark      	DB 접속 상태 체크 함수
* @param        
* @return       접속 가능 여부
*/

int dbconnok()
{
  loaderLog( L_DEBUG, "DB connection checking... : %d", PQstatus(conn));
    if (PQstatus(conn) != CONNECTION_OK) {
        setstatus(DB_CONN_ERR);
        dbstatus();
        return 0;
    }

    setstatus(DB_CONN_OK);
    dbstatus();
    return 1;
}

int dbreset()
{
    PQreset(conn);
    return 1;
}

void dbclose()
{
    if (conn != NULL) {
        PQfinish(conn);
        conn = NULL;
    }
}

/**
* @author       sclee
* @date         2014-07-04
* @remark      	DB 쿼리 함수
*				결과값이 필요없는 insert delete 등에 사용
* @param      	쿼리문 
* @return       쿼리 성공 여부
*/

inline int dbexec_no_result(const char *sql_fmt, ...)
{

    pthread_mutex_lock(&lock_db);
    PGresult *res;
    va_list ap;

    va_start(ap, sql_fmt);
    vsprintf(sql_buf, sql_fmt, ap);
    va_end(ap);


    res = PQexec(conn, sql_buf);

    if (NULL == res || PGRES_FATAL_ERROR == PQresultStatus(res)
        || PGRES_EMPTY_QUERY == PQresultStatus(res)) {
        //loaderLog(LOG_FD,
        //      "Query fails without return [%s] : [error code: %d]",
        //      sql_buf, PQresultStatus(res));
        setstatus(DB_QUERY_FAIL);
        PQclear(res);
        return (0);
    }

    PQclear(res);

    setstatus(DB_QUERY_OK);

    pthread_mutex_unlock(&lock_db);
    return (1);
}

/**
* @author       sclee
* @date         2014-07-04
* @remark       DB 쿼리 함수
*               결과값이 필요한 select 문 등에 사용
* @param        쿼리문
* @return       결과 구조체
*/

inline PGresult *dbexec_result(const char *sql_fmt, ...)
{

    pthread_mutex_lock(&lock_db);
    PGresult *res;
    va_list ap;

    va_start(ap, sql_fmt);
    vsprintf(sql_buf, sql_fmt, ap);
    va_end(ap);


    //  loaderLog( LOG_FD, "dbexec_result : [%s]", sql_buf);
    res = PQexec(conn, sql_buf);


    if (NULL == res || PGRES_FATAL_ERROR == PQresultStatus(res) ||
        PGRES_EMPTY_QUERY == PQresultStatus(res)) {
        //loaderLog(LOG_FD,
        //      "Query fails with return [%s] : [error code: %d:%s]",
        //      sql_buf, PQresultStatus(res), PQerrorMessage(conn));
        setstatus(DB_QUERY_FAIL);
        PQclear(res);
        return (NULL);
    }

    setstatus(DB_QUERY_OK);
    DB_RESULT_CNT++;

    pthread_mutex_unlock(&lock_db);
    return (res);
}

inline void dbexec_release(PGresult * res)
{
    if (res != NULL) {
        //loaderLog(LOG_FD,
        //      "There are %d more result set left in unfreed.",
        //      DB_RESULT_CNT - 1);
        DB_RESULT_CNT--;
        PQclear(res);
    }
}

