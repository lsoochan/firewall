#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <netinet/in.h>
#include <sys/types.h>

#define PORT 7071

struct _message {
    u_int32_t magic_num;
    u_int32_t message_size;
    u_int32_t request_id;
    u_int32_t flag;
    u_int32_t file_size;
    u_int32_t file_path_size;
    char file_path[1024];
};


int main()
{
	struct _message *msg;
	struct sockaddr_in logger_addr;
	static unsigned int socket_to_loader;
	int size = sizeof(struct _message) + 1024;

	msg = malloc(sizeof(struct _message) + 1024);
	if (NULL == msg) {
		printf("msg malloc failed\n");
		return 0;
	}

	msg->magic_num = 1;
	msg->message_size = 2;
	msg->request_id = 1;
	msg->flag = 1;
	msg->file_size = 3724813;
	strcpy(msg->file_path, "/temp/Firewall/udp_test/5909875005681");
	msg->file_path_size = strlen(msg->file_path);

	if ((socket_to_loader = socket(PF_INET, SOCK_DGRAM, 0)) < 0) {
		printf("socket failed\n");
		return 0;
	}


	logger_addr.sin_family = AF_INET;
	logger_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

	logger_addr.sin_port = htons(PORT);

	printf("sent size : %d\n",	sendto(socket_to_loader, msg, size, 0, (struct sockaddr *) &logger_addr, sizeof(logger_addr)));

	close(socket_to_loader);


	return 0;
}
